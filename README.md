*Wandersocke*

Die Wandersocke ist ein Liederbuchprojekt, an dem gerne mitgearbeitet werden kann.
Das Ziel der Wandersocke ist ein bündisches Liederbuch, welches sich stetig weiter entwickelt, indem neue Lieder hinzukommen.

Dabei werden verschiedene Ziele verfolgt:
Das Liederbuch soll
* stets erweiterbar sein, ohne das sich die Seitenzahlen ändern
* stets nach Liedanfang sortiert sein
* Seitenzahlen anderer gebräuchlicher Liederbücher angeben
* nach Möglichkeit Noten mit angeben
* nach Möglichkeit genaue Autorenangaben beinhalten
* alle Strophen enthalten, die von Liedern kursieren
* über allen Strophen Akkorde stehen haben
* so gedruckt werden, dass in einem Lied nie geblättert werden muss

Aber das Hauptziel ist es, ein schönes Liederbuch zu erhalten, mit dem man gerne zu Singerunden aller Art geht und Spaß hat :-)

Das Liederbuch ist in LaTex geschrieben und die Noten werden mit Lilypond gesetzt.
Bei größeren Druckmargen, bzw wenn einige Lieder eingeflossen sind und nochmal eine Nachkontrolle stattgefunden hat, wird eine Zwischenversion festgehalten.
Trotzdem soll es immer möglich sein, die gerade aktuelle Version auszudrucken.

Dafür wird darauf geachtet, dass die Datei fullbook.pdf immer den aktuellsten Stand widerspiegelt.
Genau geprüft, ob ein Druck sauber durchläuft (inkl. Doppelseiten etc.) wird jedoch nur für die Zwischenversionen.

Aktuelleste Dateien: https://gitlab.com/Wandersocke/Liederbuch/tree/master

Letzte geprüfte Version: https://gitlab.com/Wandersocke/Liederbuch/blob/2bb59e5c745c570305b8b060760d76c4fb077176/fullbook.pdf