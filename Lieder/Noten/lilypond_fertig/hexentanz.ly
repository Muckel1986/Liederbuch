\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 3/4
  %\tempo 4 = 90
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  g2 g4 d d2 b1:m b8:m e4.:m g4 g2 d d4 c c2 b1:7 b2:7 b:m b4:m e:m e2:m b:m b4:m e:m e2:m 
  g d4 e:m d2 e4:m e8:m d d4 e2:m e4:m g g8 d d4 e:m e8:m d4. e4.:m d4 e8:m
}

mel = \relative c' {
  \global
  <<
    {
      \voiceTwo
      \set midiInstrument = #"piccolo"
      \repeat volta 2 {
        g'4 g8 g4 g8 |
        fis4 fis8 fis4 fis8 |
        r8 b, d fis4 fis8 |
        fis g fis e4.
      }
      g4 g8 g4 g8 |
      a4 fis8 fis4. |
      g4 g8 g4 g8 |
      fis4 c8 c4. |
      
      s4 s s | \break
      
      b8 b dis fis g fis |
      e4. e4. |
      c8 c dis fis g fis |
      e4. e4. |
    }
    \new Voice = "two" {
      \voiceOne
      \set midiInstrument = #"clarinet"
      \repeat volta 2 {
        b'4 b8 b4 b8 |
        d4 d8 d4 d8 |
        c4 c8 c4 c8 |
        c b a b4. |
      }
      b4 b8 b4 b8 |
      d4 a8 a4. |
      c4 c8 c4 c8 |
      b4 <b dis> <b dis> |
      
      \set midiInstrument = #"glockenspiel"
      b^\markup {\upright  "Trommel"} b8 b4. |
      
      \set midiInstrument = #"clarinet"
      r2 r4 |
      b8 b b b a g |
      b4. b8 b a |
      b4. b4. |
    }
  >>
  \voiceOne
  \set midiInstrument = #"acoustic guitar (steel)"
  g8 ^\markup {\upright  "Zwischenspiel"} a g fis e fis |
  g a g e4. |
  g8 fis e fis g fis |
  e4. e4 r8 |
  <d g> <e a> <d g> <cis fis> <b e> <cis fis> |
  <d g> <e a> <d g> <cis fis>4 r8 |
  <d g> <cis fis> <a e'> <cis fis> <d g> <cis fis> |
  <c e>4. ^\markup {\upright  "1. u 2."} <b e>4 r8 \bar "||" |
  <b e>4. ^\markup {\upright  "3."} r4 r8 \bar "|."
}

vers = \lyricmode {
  E -- feu rankt um Mis -- tel -- ei -- chen, 
  kal -- ter Nacht -- wind schm -- eckt nach Rauch.
  Feucht und schau -- rig liegt das Moor, Trom -- mel -- schlag klingt
  leis ans Ohr; Feu -- er -- schein lockt in der Fer -- ne. 
  Feu -- er -- schein lockt in der Fer -- ne.
}

verse = \lyricmode {
  \set stanza = #"1. " Stumm in Moor -- lands Spie -- gel -- tei -- chen
  zit -- tert Schilf im leb -- lo -- sen Hauch.
  " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " "
  \set associatedVoice = "two"
  Feu -- er -- schein lockt in der Fer -- ne, in der Fer -- ne.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \verse}
      \addlyrics {\set fontSize = #+2 \vers}
   >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
    \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






