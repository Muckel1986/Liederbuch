\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 3/4
  %\tempo 4=100
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  c1 c c c2. e2.:m a2.:m e2.:m f2. c2. d2.:m c2. 
  g2. g2.:7 
  c2. e2.:m a2.:m e2.:m f2. c2.
  e2. e2.:7 f2. c1 g2. a2:m d2.:m a2.:m e2.:7 a2.:m
  d2.:m a2.:m e2.:m f2. d2.:m a2.:m e2.:7 a2.:m
}

mel = \relative c' {
  \global
  c8^\markup {\upright  "Intro"}
  e' e, e' g, e' |
  c, e' e, e' g, e' |
  c, e' e, e' g, e' |
  c, e' e, e' g, e' \bar "||" | \break

    <c, e g>4. e8 g4 |
    <b, e g>2 b8( b) |
    <a e' a c>4. b'8 a4 |
    <e, e' g b>4 e' e |
    <f, f' a c>4. g'8 f4 |
    <c e g>2 e8( e) |
  

      <d f a>4. e8 d4 |
      <a e' a c>4 c c8 c |
      <b g' b>2 <c c'>4 |
      <g b d f'>2.
 
     <c e g>4. e8 g4 |
    <b, e g>2 b8 b |
    <a e' a c>4. b'8 a4 |
    <e, e' g b>4 e' e |
    <f, f' a c>4. g'8 f f |
    <c e g>2 e4 |
 
      <e, e' gis b e>4. e'8 d4
      <e, e' gis b d>2.

  
  \repeat volta 2 {
    <f' c' f c'>4^\markup {\upright  "Kehrvers"}( <c' f b> <c f a>) |
    <c, e g c g'>2.|
    <g'' c>4 <g,, b d b' g' d'>4. <f'' c'>8 |
    <e b'>4 <a,, a' c e a>2 |
    <a' d f>2 f'8 f |
    <a, d f>4 <a c e>8 <a c e> <a c e> <a c e> |
    <f, e' b' e gis>4 <f e' b' e gis> <d'' gis> |
    <a, a' c e a>2. |
  }
  \break
  r2. r r r r r r r
}

vers = \lyricmode {
  " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " 
  \set stanza = #"1. "
  Ei -- si -- ges Land von Feu -- ern durch -- zo -- gen 
  und man -- chem be -- kannt 
  als In -- sel   
  der Her -- zen
  na -- mens Thu -- le -- strand. 
  Glaub ich da -- ran, was die 
  Al -- ten er -- zähl -- en 
  so le -- ben dort noch heu -- te  Trol -- le und Feen.
  Is -- land, du En -- de
  der Welt, bist Nord -- lands In -- sel von der man ger -- ne
  er -- zählt.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






