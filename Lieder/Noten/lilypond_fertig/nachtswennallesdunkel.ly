\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a1:m d a:m d a:m d2 c e1:m e:m g d c e:m g d c e:m
  g d c e:m
}

mel = \relative c' {
  \global
  a'2 r8 c d e |
  fis2. d4 |
  a2 r8 c d e |
  fis1 |
  a,2 r8 c d e |
  fis2. d4 |
  fis1( |
  fis2) r |
  g,4^\markup {\upright  "Kehrvers"} g8 g g4 g8 g |
  fis fis fis g( g4) fis |
  e8 e e fis g4 e |
  e4. dis8 e4 r |
  b'8 b b b4. b8 b |
  a a a b4. a4 |
  g8 g g a b4 g |
  g4. fis8 g4 r |
  \repeat volta 2 {
    b8. a16 g8 a b4 r |
    a8. g16 fis8 g a4 r |
    g8. fis16 e8 fis g4 e |
    e dis8 e4. r4 |
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Nachts wenn al -- les dun -- kel,
  fahl des Mon -- des Schein fühlt sich man -- cher Mensch al -- lein.
  Dann kom -- men wir mit den fro -- hen Gi -- tar -- ren und wir
  brin -- gen vie -- le Freun -- de mit.
  Ein -- sam -- keit flieht und wir sin -- gen ein Lied, das je -- der 
  kennt und al -- le sing -- en mit.
  So geht un -- ser Lied, komm und sing es mit, la -- di,
  la -- di, la -- di, la -- di -- lei.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        } <<
          \new Voice = "first"
            { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






