\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  c1 g1:7 c:1 g1:7 a1:m e1 a2:m e2 a1:m
}

mel = \relative c' {
  \global
  c4 e g e | 
  d8( e) d( c) b4 g4 |
  c4 e g e |
  d4 e8( f)e4 d |
  a4 c e c  |
  b8( c) b( a) b4 b |
  a8( b) c( d) e4 e |
  a,2 a4 r4
  \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Üb -- er mei -- ner Hei -- mat Frühl -- ing seh ich Schwä -- ne nord -- wärts flie -- gen.
  Ach, mein Herz möcht sich auf grau -- en Eis -- meer -- wog -- en wie -- gen. 
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






