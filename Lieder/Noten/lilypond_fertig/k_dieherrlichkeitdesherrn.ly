\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 4/4 
  \partial 64*32
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  s2 f1 c:7 b b2 f f1 c:7 b b2 f f1 c:7 b b2 f f1 c:7 b f2
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 c2 ^\markup {\upright  "1"} | % 2
	 f4  f  g  f | % 3
	 e2  f4  e | % 4
	 d2.  d4 | % 5
	 c2\fermata  f4( ^\markup {\upright  "2"} g) | % 6
	 a2  bes4  a | % 7
	 g2  a4  g | % 8
	 f2.(  g4) | % 9
	 f2\fermata a4 ^\markup {\upright  "3"} bes | % 10
	 c2  c4  c | % 11
	 c2.  c4 | % 12
	 bes2  c4(  bes) | % 13
	 a2\fermata  f4 ^\markup {\upright  "4"} g | % 14
	 a  a  bes  a | % 15
	 g2  a4(  g) | % 16
	 f2.  g4 | % 17
	 f2  
	}
	
}

verse = \lyricmode{
	 Die   Herr -- lich -- keit   des   Herrn   blei -- be   e  
	 -- wig -- lich.   Der      Herr   freu -- e   sich   sei  
	 -- ner   Wer      -ke.   Ich   will   sin -- gen   dem  
	 Herrn   mein   Le -- ben      lang,   ich   will   lo  
	 -- ben   mei -- nen   Gott   so      lang   ich   bin.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}