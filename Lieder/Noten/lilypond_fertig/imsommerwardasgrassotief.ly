\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={ 
  \time 4/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 a1:m d1:m g1 c1 a2:m e2:m g1 f1 a1:m
}

melody = \relative c' {
  \global
  \repeat volta 2 {
	 e4 | % 2
	 a  b8  c4.  a4 | % 3
	 d  e8  f4  d8  d  d | % 4
	 g4  g8  g4  d8  e  f | % 5
	 e2 r8   e  e  e | % 6
	 a4  a  g  e8  d ~  | % 7
	 d2 r8   b  b  c | % 8
	 d4  e8  c4  b8  a  g | % 9
	 a1 
       }
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Im   Som -- mer   war   das   Gras   so   tief,   dass  
	 je -- der   Wind   da -- ran   vo -- rü -- ber   lief.  
	 Ich   ha -- be   da   dein   Blut   ge -- spürt   und  
	 wie   es   heiß   zu   mir   he -- rü -- ber -- rann.  
	
}
verse = \lyricmode{
	 Du   hast   nur   mein   Ge -- sicht   be -- rüht,   da  
	 starb   er   ein -- fach   hin,   der   har -- te   Mann,  
	 weils   sol -- che   Lie -- be   nicht   mehr   gibt,   ich  
	 hab   mich   in   dein   ro -- tes   Haar   ver -- liebt.  
	
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+1 \vers}
      \addlyrics {\set fontSize = #+1 \verse}
    >>
    \layout { }
  }
}
