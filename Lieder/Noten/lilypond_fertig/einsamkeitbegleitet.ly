\version "2.16.2"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 3/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a2.:m e2.:m a2.:m e2.:m c2. d2.:m
  e2.:m c4 e:m a:m
  e1:m g2 d e1:m g2 d 
  e1:m g2 d e1:m 
  g4 d e2:m
}

mel = \relative c' {
  \global
  a4. (a8) b8 c |
  d e4 c b8 |
  a4. b8 c d |
  e4 f e |
  d4. f8 e d |
  e4 c a 
  b4. c8 b a |
  c4 b a \bar "||" |

  \set Staff.explicitKeySignatureVisibility = #end-of-line-invisible
  \once \override Staff.TimeSignature #'break-visibility = #begin-of-line-visible
  \key g \major
  \time 4/4
  e'8^\markup {\upright  "Kehrvers"} e e d e4 b |
  g' a8 g fis2 |
  e8 e e d e4 b |
  g' a8. g16 fis8( e8) d4 | 
  e8 e e d e4 b |
  g' a8( g) fis2 | 
  e8 e e d e4 b | 
  g' fis e r4 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Ein -- sam -- keit be -- glei -- tet uns,
  wir sind wie -- der un -- ter -- wegs.
  Zie -- hen hin zu fer -- nem Land 
  neu -- en Wun -- der -- wei -- ten zu.
  Wei -- ter, wei -- ter, wei -- ter führt uns der Weg,
  hö -- her, hö -- her, hö -- her, wol -- len wir stei -- gen,
  im -- mer, im -- mer vor -- wärts, hin zum Licht,
  in der wei -- ten Fer -- ne sind wir frei.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        } <<
          \new Voice = "first"
            { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






