\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/2
  %\tempo 4 = 110
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  c1 g a:m e:m f g c g c g a:m e:m f g2. g4:7 c1
}

mel = \relative c' {
  \global
  \set midiInstrument = #"flute"
  e2 f4 g g2 d |
  c d4 e e1 |
  a,2 b4 c d2 c4 d |
  e2 g d r |
  e2 f4 g g2 
  <<
    {
      \voiceOne
      \set midiInstrument = #"clarinet"
      b2 |
      c b4 a8( g) g1 |
      f2 f4 f f2. e4 |
      e1 g1\rest
    }
    \new Voice {
      \voiceTwo
      \set midiInstrument = #"flute"
      d2 |
      c d4 e e1 |
      a,2 b4 c d2. c4 |
      c1 s \bar "|."
    }
  >>  
}

vers = \lyricmode {
  \set stanza = #"1. " Reicht euch die Hand und sagt: ''Gu -- te Nacht'', 
  hat doch das Tag -- werk euch müd ge -- macht.
  Nun ist es Zeit, von al -- lem zu ruhn,
  die Las -- ten ab -- zu -- tun.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






