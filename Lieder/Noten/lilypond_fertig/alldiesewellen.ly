\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \germanChords
  \set chordNameLowercaseMinor = ##t
  s2 a1:7 d:m c f4 d:7 
  d2:7 g1:m d2:m s8 bes bes4 g2:m a d4:m d:7 a2:7 d:m
}

mel = \relative c' {
  \global
  r8 f e d a'4 g |
  r8 g f e g4 f |
  r8 d' d d c4 g |
  c bes a2 | \break
  \repeat volta 2 {
    r8 c c c c4 bes |
    r8 <g d'> <f d'> <e d'> <bes' d>4 a |
    r8 f e d a'4 <g bes> |
  }
  \alternative {
    {f g a r}
    {f cis d r \bar "|." }
  }
}

vers = \lyricmode {
  \set stanza = #"1. " All die -- se Wel -- len, ja die -- se Wel -- len;
  die sol -- len bloß zur Höl -- le fahr'n. Und kei -- ne Kar -- ten von die -- sen Stel -- len,
  ich trei -- be vor -- wärts oh -- ne Plan. oh -- ne Plan.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






