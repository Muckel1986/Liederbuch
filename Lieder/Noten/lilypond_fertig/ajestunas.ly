\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 d1:m d:m d:m d:m d:m d:m 
  g:m g:m g:m g:m d:m d:m a:m
  a:m d:m d:7 a:m d:m g:m g:m
  d:m d:m a:m a:m d:m d:7 a:m 
  d2:m
}

mel = \relative c' {
  \global
  \partial 4 
  a'4 |
  a r8 a a4 r8 a |
  a4 f e f |
  e d r2 |
  r2. d'4 |
  d4. d8 d4 r8 d |
  d4. d,8 e4 f |
  g2. r4 |
  r2. 
  \repeat volta 2 {
    bes4 |
    c4. bes8 bes4 r |
    c4. bes8 bes2 |
    bes4. a8 a4 d, |
    bes'4. a8 a2 |
    a4. g8 g4 bes |
  }
  \alternative {
    {
      a g f g |
      a1 ( |
      c2. ) s4
    }
    {
      a4 g f e |
      d1 |
    }
  } 
  \repeat volta 2 {
    c'4.^\markup {\upright  "Kehrvers"}
    ( bes8 ) bes4 bes |
    c bes bes r |
    bes a a d, |
    bes' a r a8 a |
    a4 g g bes |
  }
  \alternative {
    {
      a g f g |
      a1 ( |
      c ) |
    }
    {
      a4 g f e |
      d2. \bar "|."
    }
  }
}

vers = \lyricmode {
  \set stanza = #"1. " A jest u nasw 
  Ra -- jon -- je mal -- ta -- wan -- ki.
  Ech, u -- li -- ca isw -- jest -- na -- ja, 
  dru -- sja. A sta -- ren kije 
  dwor -- ni -- ki pad -- me -- ta -- li
  dwo -- ri ki, schto ws -- ja la u -- li -- ca
  ma -- ja. u -- li -- ca ma -- ja.
  U -- li -- ca, u -- li -- ca, u -- li -- ca
  rad -- na -- ja. Mas -- sa -- je -- row -- ska
  ja u -- li -- ca ma -- ja. u -- li -- ca ma -- ja.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






