\version "2.12.3"

%\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key d \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  b1:m g fis:m a2 g1 a g fis2:m b:m g4 a d1
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  b4^\markup {\upright  "1. 2. + 4. Strophe"} d fis b |
  cis d cis b |
  a b cis a |
  \time 2/4
  e fis |
  \time 4/4
  d2 r | \break
  \repeat volta 2 {
    e4 fis g a |
    g fis e d |
  } \break
  e fis d a |
  \time 2/4
  b cis |
  \time 4/4
  d2 r \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Wo die le -- eren Stock -- ge -- lei -- se sich ins Nutz -- land
  wie ein Keil schie -- ben, blüht aus Schutt und Ril -- le, um die Schwel -- len blaß und heil.
}

verse = \lyricmode {
 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ kies -- ge -- säu -- ert die Ka -- mil -- le,
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
    \new Staff
    \new Voice <<
      \mel
   >>
   
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
      
   >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






