\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key c \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d1:m bes c d:m d:m bes c a f c d:m a
  d:m bes c d:m d:m bes c2 c:7 a a:7 d1:m bes c2 a d1:m
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  d4 d8 e f4 d |
  f1 |
  c2 d4 e |
  d1 |
  d4 d8 e f4 e |
  g f e d |
  c2 b |
  a2 r |
  a'4 f8 g a4 b |
  g2 c |
  f,4 d8 e f4 g |
  e1 |
  d4 d8 e f4 e |
  f e d c |
  c2 e4( d) |
  d1 |
  \repeat volta 2 {
    d4^\markup {\upright  "Kehrvers"} d8 e f4 e |
    g f e d |
    c2 b |
    a1 |
    d4 d8 e f4 e |
    g f e d |
    c2 e4( d) |
    d1 |
  }
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Ü -- ber dem Ho -- ri -- zont rot Son -- ne thront,
  na -- hen zwei Rei -- ter auf der Stra -- ße nach Cler -- mont.
  Wie durch das bun -- te Laub es sie in die Wäl -- der zieht,
  singt in den kah -- len Zwei -- gen leis der Wind sein Lied.
  Bam ba -- da bam bam ba -- ba bam bam ba ba ba, 
  bam ba -- da bam bam ba -- da bam bam ba ba ba.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






