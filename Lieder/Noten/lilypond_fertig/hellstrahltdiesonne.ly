\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d1 b:m e:m a:7 d b:m g 
  \set chordChanges = ##f
  a:7 a:7 
  \set chordChanges = ##t
  g fis:m e2:m a:7 d d:7 g1 fis:m e2:m a:7 d1
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    d2. cis8 d |
    b2. cis8 d |
    e2. d8 e |
    cis2. d8 e |
    fis2. e8 fis |
    d2. e8 fis |
    g4 fis e8 d4. |
  }
  \alternative {
    {
      e1 |
    }
    {
      a2 r |
    }
  }
  \repeat volta 2 {
    \bar "||"
    r4^\markup {\upright  "Kehrvers"}
    b2 b4 |
    a fis8 a4.( a4) |
    r g fis e |
    fis g8 a4.( a4) |
    r b b b |
    a fis8 a4.( a4) |
    r g fis8 fis4. |
    d1 | 
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Hell strahlt die Son -- ne, der Tag ist er -- wacht,
  und die Vö -- gel, sie sin -- gen so fröh -- lich nach der Nacht. " "
  Herr, ich lo -- be Dich! Denn Du er -- wärmst auch mich. Dein Licht ist für mich da.
  Hal -- le -- lu -- ja!
}

verse = \lyricmode {
  Licht leuch -- tet auf und durch -- flu -- tet die Welt, die Na -- tur spie -- gelt Glanz
  und das Dun -- kel ist er - hellt.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






