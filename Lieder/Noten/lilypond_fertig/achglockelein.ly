\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 a2:dim a:m a2:dim a:m a2:dim a:m a2:dim a:m 
  a1:m a:m a:m a:m a2:m a:7 d1:m d:m a:m e a2:m a:7
  d1:m a:m e a2:m
}

mel = \relative c' {
  \global
  \partial 4
  f'8 e |
  dis4 c8 b a4 f'8 e |
  dis4 c8 b a4 f'8 e |
  dis4 c8 b a4 f'8 e |
  dis4 c8 b a2 |
  c e |
  a4. r2 e,8 | \break
  e16 a a4 a8 gis gis g g |
  f4( e) r e8 e |
  e e e8. e16 f8 e e e |
  d2 r8 d d d |
  e d d d e d d d |
  c4 r r8 c c c |
  b b8. b16 b8 c b c d |
  e2( g8) g g g |
  g f f f g f f f |
  e4 r r8 e e e |
  e d d d e d c b |
  a4 r r \bar "|."
}

vers = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
  \set stanza = #"1. " Ach, Glö -- cke -- lein, die klin -- gen zart, bu -- bu,
  und die Ar -- beit ist für mich heu -- te ta -- bu.
  Lass die Ma -- schi -- nen rat -- tern, denn ich bleib' im Bett,
  noch fünf Mi -- nüt -- chen, sonst fühl' ich mich gar nicht fit.
  Lass die Ma -- schi -- nen rat -- tern, denn ich bleib' im Bett,
  noch fünf Mi -- nüt -- chen, sonst fühl' ich mich gar nicht fit.
}

versr = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
  \set stanza = #"1. " Ах, коло -- кол -- ьчики -бу -- бе -- нчи -- ки, бу -- -бу,
  а на ра -- боту я се -- го -- дя не пой -- ду.
  Пус -- кай ра -- бо -- та -- ет жел -- езн -- ьій па -- ро -- воз,
  Не для ра -- бо -- тьі он ме -- ня сю -- да при -- вёз.
  Пус -- кай ра -- бо -- та -- ет жел -- езн -- ьій па -- ро -- воз,
  Не для ра -- бо -- тьі он ме -- ня сю -- да при -- вёз.
}

verst = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
  \set stanza = #"1. " Ach, kolo -- kol -- tschiki -- -bu -- ben -- tschi -- ki bu -- -bu,
  a na ra -- botu ja se -- vo dnja nje pai -- du.
  Puss -- kaj ra -- bo -- ta -- jet žel -- jes -- nij pa -- ra -- vos,
  nje dlja ra -- bo -- ty on men -- ja sju -- da pri -- vjos.
  Puss -- kaj ra -- bo -- ta -- jet žel -- jes -- nij pa -- ra -- vos,
  nje dlja ra -- bo -- ty on men -- ja sju -- da pri -- vjos.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
    \new Staff
    \new Voice <<
      \mel
   >>
   
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \versr}
      \addlyrics {\set fontSize = #+2 \verst}
   >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






