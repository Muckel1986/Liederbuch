\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
    \clef treble
    \key d \major 
    \time 6/8 
    \partial 64*8
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 s8 c2 c4 g2 g4 a1:m a2:m f2 f4 d2:m d4:m g1 g2
 c2 c4 g2 g4 a1:m a2:m f2 f4 c4 c8 f4 f8 c1 c1 c4
 g2 g4 a1:m a2:m f2 f4 a2:m a4:m d2:m d4:m g2 g4 c2 
 c4 g2 g4 a1:m a2:m f2 f4 g4 g8 f4 f8 c4 c8 f4 f8 c
}

melody = \relative c' {
  \global
	 a8 | % 2
	 d4  d8  d4  d8 | % 3
	 e4  e8  fis4  e8 | % 4
	 e4.  d | % 5
	r4 . r4   d8 | % 6
	 g4  g8  g4  g8 | % 7
	 g4  fis8  e4  d8 | % 8
	 e2. | % 9
	r4 . r4   a,8 | % 10
	 d4  d8  d4  d8 | % 11
	 e4  e8  fis4  e8 | % 12
	 e4.  d | % 13
	r4 . r4   fis8 | % 14
	 g4  g8  g4  g8 | % 15
	 fis4  fis8  e4  e8 | % 16
	 d2. 
	\bar "||"
	\break
	r4. ^\markup {\upright  "Kehrvers"} r4   fis8 | % 18
	 a4  a8  a4  a8 | % 19
	 a4  g8  fis4  e8 | % 20
	 d4.  d | % 21
	r4   fis8  g4  a8 | % 22
	 b4  b8  b4. | % 23
	r4   a8  g4  fis8 | % 24
	 e2. | % 25
	r4 . r4   fis8 | % 26
	 fis4  a8  a4  fis8 | % 27
	 a4  a8  fis4  e8 | % 28
	 d4.  d | % 29
	r4 . r4   d8 | % 30
	 d4  d8  e4  fis8 | % 31
	 e4  d8  b4  d8 | % 32
	 d2. ~  | % 33
	 d2 r8 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Von   gu -- ten   Mäch -- ten   treu   und   still   um  
	 -- ge -- ben,   be -- hü -- tet   und   ge -- trö -- stet  
	 wun -- der -- bar,   so   will   ich   die -- se   Ta  
	 -- ge   mit   euch   le -- ben   und   mit   euch   ge  
	 -- hen   in   ein   neu -- es   Jahr.   Von   gu -- ten  
	 Mäch -- ten   wun -- der -- bar   ge -- bor -- gen,   er  
	 -- war -- ten   wir   ge -- trost,   was   kom -- men   mag.  
	 Gott   ist   bei   uns   am   A -- bend   und   am  
	 Mor -- gen   und   ganz   ge -- wiss   an   je -- dem  
	 neu -- en   Tag.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}