\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 d1:m d:m d:m a a a a d:m d:m g:m d:m e2:7 a d:m a d:m g:m d:m a d:m
}

mel = \relative c' {
  \global
  \partial 4
  a4 |
  d d8 e f4 f8 e |
  d4 d8 e f4 r |
  d d8 e f f e d |
  e4 a, a r8 a |
  e'4 e8 f g4 g8 f |
  e4 e8 f g4 r8 f |
  e4 e8 e a g f e |
  f4 d d2 |
  a'4 a a a8 a |
  bes4 bes a g8 g |
  f4 f e d |
  e a, a2 |
  d4 f8 f e4 g |
  f a8 a g4 bes |
  a f8 f e4 f |
  d2. a4 |
  d2. a4 |
  d2. \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Der jüng -- ste und bes -- te Ma -- tro -- se
  an Bord, das war der klei -- ne Kal -- le The -- o -- dor.
  Die Mut -- ter, die wein -- te, die ließ ihn nicht fort,
  doch ihn rief die See den klei -- nen The -- o -- dor.
  Wenn der Sturm in den Mas -- ten to -- bte, dann schrie er laut:
  Ah -- oi. oh -- e! Hat -- te kein Heim -- weh, denn sei -- ne
  Hei -- mat war jetzt die wei -- te See. Ah -- oi, oh -- e.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






