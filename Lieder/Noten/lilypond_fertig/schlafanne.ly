\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 3/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  b2:m b4:m e:m e2:m fis:7 fis4:7 b1:m b2:m a:7 a1:7 d4 d2
  e:m e1:m fis2:7 fis4:7 b1:m b2:m fis1:7 fis2:7 b:m b4:m
}

mel = \relative c' {
  \global
  b4 fis' d |
  e4. d8 cis4 |
  e4 cis4. d8 |
  b4 r2 |
  b4 fis' d |
  e e4. e8 |
  e4 fis g |
  fis r2 |
  \repeat volta 2 {
    b4 g b |
    g4.( fis8) e4 |
    cis d4. e8 |
    fis4 r2 |
    fis4 g fis |
    e4. d8 cis4 |
    e cis4. d8 |
    b4 r2 |
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Schlaf, An -- ne, schlaf nur ein, bald kommt die Nacht,
  hat sich aus Wol -- ken Pan -- tof -- feln ge -- macht, kommt von den Ber -- gen,
  kommt von ganz weit, schlaf An -- ne, schlaf nur ein, s'ist Schla -- fens -- zeit.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        } <<
          \new Voice = "first"
            { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






