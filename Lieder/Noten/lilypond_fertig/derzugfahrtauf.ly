\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 e1:m a2:m b:7 e1:m a2:m b:7 a1:m e:m d g2 e:m a:m b:7 e1:m
  a:m e:m a:m e:m a:m e:m a2:m b:7 e:m
}

mel = \relative c' {
  \global
  \partial 4
  b4 |
  e2 |
  g4 e |
  a8 g4 fis8 |
  a8 g4 fis8 |
  e2 |
  g4 e |
  a8 g4 a8 |
  b4 b |
  c2 |
  a4 c |
  b8 b4 a8 |
  b4 g8 g |
  a2 |
  d,4 d |
  b'8 b4 a8 |
  b4 g8 g |
  a8 a4 a8 |
  fis g4 fis8 |
  e2 ( |
  e ) |
  a4^\markup {\upright  "Kehrvers"} a |
  e8 e e fis |
  g4 g |
  e e |
  a4. a8 |
  e e e fis |
  g g4 b8 ( |
  b4 ) b8 b |
  c4 c8 c |
  c4 a8 c |
  b b b g |
  e e r4 |
  a8 a r4 |
  r g8 ( fis ) |
  e2 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Der Zug fährt auf stäh -- ler -- nen 
  Glei -- sen, die ha -- ben wir sel -- ber ge -- legt, dass sie auf 
  den end -- lo -- sen Rei -- sen ins Mor -- gen die Rich -- tung
  uns wei -- sen und dass un -- ser Zug sich be -- wegt.
  Denn wir müs -- sen al -- le wei -- ter -- kom -- men.
  Und da dür -- fen wir nicht zag -- haft sein.
  Je -- des Ziel, kaum er -- reicht, ist schon wie -- der weg -- 
  ge -- schwom -- men. Al -- so, heizt ein!
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






