\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4 
}

melody = \relative c' {
  \global
	 r2  r4 .  c8 | % 2
	 d4 ^\markup {\upright  "1"} d  a'2 | % 3
	 g8  f  e  d  e4 r4  | % 4
	 f ^\markup {\upright  "2"}  f8  f  c'4  c | % 5
	 b8  c  d  b  c4 r8   c | % 6
	 d4 ^\markup {\upright  "3"} a  f2 | % 7
	 g8  g  g  d  g4 r4  | % 8
	 a ^\markup {\upright  "4"} f8  e  d4  c | % 9
	 d8  c  b  d  c4 r4  
	\bar "|."
	
}

verse = \lyricmode{
	 Der   Him -- mel   geht   ü -- ber -- al -- len   auf,  
	 auf   al -- le   ü -- ber,   ü -- ber   al -- len  
	 auf.   Der   Him -- mel   geht   ü -- ber   al -- len  
	 auf,   auf   al -- le   ü -- ber,   ü -- ber   al  
	 -- len   auf.  
}

\book {
  \score {
    <<
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
