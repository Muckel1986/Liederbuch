\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
  
}

chordNames = \chordmode {
  \global
  \set chordNameLowercaseMinor = ##t
  \set majorSevenSymbol = \markup { j7 }
  c1	| c:m	| cis	| cis:m	| c:7	| s	| s	|
  d 	| d:m 	| dis 	| d:7 	| d:7+ 	| s	| s	|
  e	| e:m	| e:7	| es	| e:m7	| s	| s	|
  f	| f:m 	| fis	| fis:m | f:7+	| fis:7	| s	|
  g     | g:m	| gis	| g:7	| g:6	| g:11	| s	|
  a     | a:m	| a:7	| a:m7	| a:dim	| s	| s	|
  b     | b:m	| bes	| b:7	| b:m7	| s	| s	|
}

mel = \relative c' {
  \global
  \clef "treble_8"
    \textLengthOn
    % Allgemeine Eigenschaften von Bund-Diagramme bestimmen
    \override TextScript #'size = #'2
    \override TextScript
      #'(fret-diagram-details finger-code) = #'in-dot
    \override TextScript
      #'(fret-diagram-details dot-color) = #'black
    
      
  s1 ^\markup { \fret-diagram-terse #"x;3-3;2-2;o;1-1;o;" } |
  s1 ^\markup { \fret-diagram-terse #"x;3-1-(;5-3;5-4;4-2;3-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"x;4-1-(;6-3;6-3;6-3;4-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"x;4-1-(;6-3;6-4;5-2;4-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"x;3-3;2-2;3-4;1-1;o;" } |
  \once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } |
  \once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } | \break
        
  s1 ^\markup { \fret-diagram-terse #"x;x;o;2-1;3-3;2-2;" } |
  s1 ^\markup { \fret-diagram-terse #"x;x;o;2-2;3-3;1-1;" } |
  s1 ^\markup { \fret-diagram-terse #"x;6-1-(;8-3;8-3;8-3;6-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"x;x;o;2-2;1-1;2-3;" } |
  s1 ^\markup { \fret-diagram-terse #"x;4-4;3-3;2-1-(; ;2-1-);" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } | \break
  
  s1 ^\markup { \fret-diagram-terse #"o;2-2;2-3;1-1;o;o;" } |
  s1 ^\markup { \fret-diagram-terse #"o;2-2;2-3;o;o;o;" } |
  s1 ^\markup { \fret-diagram-terse #"o;2-2;2-3;1-1;3-4;o;" } |
  s1 ^\markup { \fret-diagram-terse #"x;x;1-1;3-2;4-4;3-3;" } |
  s1 ^\markup { \fret-diagram-terse #"o;2-2;2-3;o;3-4;o;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } | \break
  
  s1 ^\markup { \fret-diagram-terse #"1-1-(;3-3;3-4;2-2; ;1-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"1-1-(;3-3;3-4; ; ;1-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"2-1-(;4-3;4-4;3-2; ;2-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"2-1-(;4-3;4-4; ; ;2-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"1-1-(;3-4;2-2;2-3; ;1-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"2-1-(;4-3; ;3-2; ;2-1-);" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } | \break
  
  s1 ^\markup { \fret-diagram-terse #"3-3;2-2;o;o;o;3-4;" } |
  s1 ^\markup { \fret-diagram-terse #"3-1-(;5-3;5-4; ; ;3-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"4-1-(;6-3;6-4;5-2; ;4-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"3-3;2-2;o;o;o;1-1;" } |
  s1 ^\markup { \fret-diagram-terse #"3-3;2-2;2-1;o;o;3-4;" } |
  s1 ^\markup { \fret-diagram-terse #"3-3;2-2;o;o;1-1;3-4;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } | \break
  
  s1 ^\markup { \fret-diagram-terse #"x;o;2-1;2-2;2-3;o;" } |
  s1 ^\markup { \fret-diagram-terse #"x;o;2-2;2-3;1-1;o;" } |
  s1 ^\markup { \fret-diagram-terse #"x;o;2-1;2-2;2-3;3-4;" } |
  s1 ^\markup { \fret-diagram-terse #"x;o;2-2;2-3;1-1;3-4;" } |
  s1 ^\markup { \fret-diagram-terse #"x;12-1;13-2;14-4;13-3;x;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } | \break
  
  s1 ^\markup { \fret-diagram-terse #"x;2-1-(;4-3;4-3;4-3;2-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"x;2-1-(;4-3;4-4;3-2;2-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"x;1-1-(;3-3;3-3;3-3;1-1-);" } |
  s1 ^\markup { \fret-diagram-terse #"x;2-2;1-1;2-3;o;2-4;" } |
  s1 ^\markup { \fret-diagram-terse #"x;2-2;o;2-3;o;2-4;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } |
\once     \override TextScript
        #'(fret-diagram-details mute-string) = #""
  s1 ^\markup { \fret-diagram-terse #"x;x;x;x;x;x;" } | \break
}

vers = \lyricmode {
  \set stanza = #"1. " 
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
        \remove "Staff_symbol_engraver"
        \remove "Clef_engraver"
        \remove "Time_signature_engraver"
        \remove ""
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
     
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
      \remove "Bar_number_engraver"
    }
    }
  }
}






