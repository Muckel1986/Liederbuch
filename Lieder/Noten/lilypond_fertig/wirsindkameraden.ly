\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 s4 f1 a:m d:m f d:m f c c
 \set chordChanges = ##f
 c
 \set chordChanges = ##t
 c f c d:m a:m f2 d:m c1 f2 c:7 f
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 \partial 4
	 g4 | % 2
	 f'4.  g  a4 | % 3
	 e4.  c  a4 | % 4
	 d4.  e  f4 | % 5
	 c2.  a4 | % 6
	 d4.  e  f4 | % 7
	 c4.  bes  c4 |
	 }
	 \alternative {
	{
	 
	 g'2.(  a4 | % 9
	 g2.) s4 |
	}
	
	{
	 g2. a4 | % 11
	 g2. \bar "||" \break
	}}
	
	\repeat volta 2 {
	 c,4 ^\markup {\upright  "Kehrvers"} | % 13
	 a'2.  g8  f | % 14
	 g4.  c8  c  bes  a  g | % 15
	 a2.  g8  f | % 16
	 g2.  c,8  c | % 17
	 a'4  a8  a  a4  g8  f | % 18
	 g  g  g  e  c4  c8  d | % 19
	 c4  f8  a  g4  a8  g | % 20
	 f2.
	}
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Wir   sind   Ka -- me -- ra -- den,   wir   hal -- ten  
	 zu -- samm'n,   e -- gal,   ob   bei   Tag   o -- der  
	 Nacht. _ _ _ Ja   frei  
	 wie   ein   Al -- ba -- tros   zi -- eh -- en   wir  
	 durch   die   Welt,   heu -- te   hier,   mor -- gen   dort,  
	 gra -- de   wie   es   uns   ge -- fällt,   ja   so  
	 frei   ist   die   Pfad -- fin -- der -- ei.  
}
verse = \lyricmode{
	 Wir   füh -- ren   die   Kro -- ne   in   un -- ser  
	 -- er   Fahn'   und   zie -- hen   im -- mer mun -- ter
	 _ uns -- re   Bahn.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
