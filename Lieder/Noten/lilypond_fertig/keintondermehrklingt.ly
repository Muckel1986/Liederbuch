\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 6/8
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  %\override ChordName #'font-series = #'bold
  \germanChords
  s4 e1.:m d1. c1. b1.:7 b2.:7 e1.:m d1. d2. e2.:m
}

mel = \relative c'' {
  \global
  \partial 4
  b,4 
  \repeat volta 2 {
    e e fis |
    g2 fis8 e |
    fis4 fis g |
    fis2 fis8 fis |
    e4 e e |
    e fis g |
    fis fis fis |
  }
  \alternative{
   { 
   fis2 b,8 b |
   }
   {
   fis'2 e8 fis |
   }
  }
  
  g4^\markup {\upright  "Kehrvers"} e2 |
  r2 g4 |
  fis4 fis2 |
  r2 r4 |
  fis e d |
  e e2
  
  \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Kein Ton, der mehr klingt, mei -- ne Stim -- me zer -- springt, al -- les sau -- ber
  und kalt wird Ge -- fühl zu Ge -- walt. Geld ist 
}

verse = \lyricmode {
  _ al -- les, was zählt und die Wen -- de ge -- wählt.
  Al -- le Stras -- sen sind fremd, ist kein Haus, das mich _ _ _ kennt.
  Ich hab Heim -- weh, nach Hei -- mat, wo das auch sein mag.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\mel}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






