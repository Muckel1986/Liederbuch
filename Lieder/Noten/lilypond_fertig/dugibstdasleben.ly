\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key a \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a1 cis:m d2 e a1 cis:m fis:m b:m e a
  cis:m d e a fis:m d a cis:m d 
  e2 d cis1:m d e fis:m cis:m
}

mel = \relative c' {
  \global
  cis2 ^\markup {\upright  "Kehrvers"} cis4 d |
  e2 e |
  fis4 e cis8 b4. |
  cis2. r4 |
  r e e fis |
  a2 a4 a |
  a fis gis a |
  b2 a4 b |
  cis cis b cis |
  gis gis cis b |
  a a gis a |
  b2 a4 b |
  cis a2 r4 |
  r2 a4 b |
  cis a a fis |
  a2. r4 \bar "||" | \break
  gis2 cis4. b8 |
  a fis( fis4) r a |
  gis gis a fis |
  gis2 r4 gis |
  fis fis b a |
  gis4. fis8 e4 d |
  cis4 cis fis a |
  gis2. r4 \bar "|." 
}

vers = \lyricmode {
  Du gibst das Le -- ben, das sich wirk -- lich lohnt.
  Für dies Ver -- sprech -- en hast du dich nicht ver -- schont.
  Und du gibst nicht nur ein wen -- ig, Herr, die Fül -- le ist bei dir.
  Du das Le -- ben, gibst das Le -- ben, das sich lohnt. 
  \set stanza = #"1. " Du gibst das Le -- ben mit ein -- em kla -- ren Sinn,
  be -- en -- dest das Ver -- lo -- ren -- sein, schenkst ein -- en Neu -- be -- ginn.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






