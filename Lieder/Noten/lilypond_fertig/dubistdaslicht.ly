\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  g1 e:m a:m d:7 g e:m a:m d:7 g e:m a:m d:7 g e:m a:m d g2 c g1
  c c g g c e:m d:7 d:7 c c g g c a:7 d d:7
}

melody = \relative c' {
  \global
	 g'8 ^\markup {\upright  "Kehrvers"} g4  a8  b  a  g4 | % 2
	 g8  g4  g8  g  fis  e4 | % 3
	 e8  e4  e8  c4  e | % 4
	 fis2. r4  | % 5
	 g8  g4  g8  b  a  g4 | % 6
	 g8  g4  g8  g  fis  e4 | % 7
	 e8  e4  e8  c4  e | % 8
	 fis2. r4  | % 9
	 b8  b4  b8  b  a  g4 | % 10
	 b2. r4  | % 11
	r8   c  c  c  c  b  a4 | % 12
	 fis2. r4  | % 13
	 b8  b4  b8  b  a  g4 | % 14
	 b2. r4  | % 15
	r8   c  c  c  c  b  a4 | % 16
	 fis2  e4  d | % 17
	 g2  g | % 18
	 g r2  
	\bar "||" \break
	 e  e4  e | % 20
	 g4.  g8  fis4  e | % 21
	 e4.  c8  b4  c | % 22
	 d2. r4  | % 23
	 e4.  e8  e4  e | % 24
	 g4.  g8  fis4.  g8 | % 25
	 a4.  g8  fis4  e | % 26
	 d2. r4  | % 27
	 e4.  e8  d4  e | % 28
	 g4.  g8  fis4  e | % 29
	 d4.  b8  b4  c | % 30
	 d2. r4  | % 31
	 e4.  e8  e4  e | % 32
	 g4.  g8  fis4  g | % 33
	 a1 ~  | % 34
	 a 
	\bar "|."
	
}

verse = \lyricmode{
	 Du   bist   das   Licht   der   Welt,   du   bist   der  
	 Glanz,   der   uns   un -- ser -- en   Tag   er -- hellt.  
	 Du   bist   der   Freu -- den -- schein,   der   uns   so  
	 glück -- lich   macht,   dringst   sel -- ber   in   uns   ein.  
	 Du   bist   der   Stern   in   der   Nacht,   der   al  
	 -- lem   Fin -- ser -- en   wehrt,   bist   wie   ein   Feu  
	 -- er   ent -- facht,   das   sich   aus   Lie -- be   ver  
	 -- zehrt,   du   das   Licht   der   Welt.   \set stanza = #"1. " So wie   die  
	 Son -- ne   stets   den   Tag   bringt   nach   der   Nacht,  
	 wie   sie   auch   nach   Re -- gen -- wet -- ter   im  
	 -- mer   wie -- der   lacht,   wie   sie   trotz   der   Wol  
	 -- ken -- mau -- er   uns   die   Hel -- le   bringt   und  
	 doch   nur   zu   neu -- em   Auf -- gehn   sinkt.  
}


\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}