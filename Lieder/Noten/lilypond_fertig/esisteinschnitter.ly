\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
 \clef treble
 \key f \major 
 \time 3/4 
 \partial 64*32
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
 s4 d2:m d4:m c2 c4 a2:m a4:m d2:m d4:m f2 f4 c2 c4 f1 f1 f1 c1 c2
 b1 b2 a1:7 a2:7 d1:m c2 d4:m
}

melody = \relative c' {
  \global
  \partial 4
	 d4 | % 2
	 d2  f4 | % 3
	 e2  d4 | % 4
	 c2  bes4 | % 5
	 a2  a'4 | % 6
	 a4.(  g8)  f4 | % 7
	 g4.(  f8)  e4 | % 8
	 f2. ~  | % 9
	 f2  a4 | % 10
	 a4.  g8  a4 | % 11
	 f4.  g8  a4 | % 12
	 g4.  f8  g4 | % 13
	 e4.  f8  g4 | % 14
	 f4.  e8  f4 | % 15
	 d4.  e8  f4 | % 16
	 e4.  d8  e4 | % 17
	 c2  a4  \time 4/4  a'2  g4  f | % 19
	 e4.  e8  d4
	 \bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Es   ist   ein   Schnit -- ter   heißt   der   Tod,   hat  
	 G'walt      vom      groß -- en   Gott.   Heut   wetzt  
	 er   das   Mes -- ser,   es   schneidt   schon   viel   bes  
	 -- ser,   bald   wird   er   drein -- schnei -- den,   wir   müs  
	 -- sens   er -- lei -- den.   Hüt   dich,   schöns   Blü -- me  
	 -- lein!     
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
