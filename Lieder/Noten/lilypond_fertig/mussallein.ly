\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  e1:m d2 e:m e:m b:7 e:m e:m e:m e:m d e:m e:m
  d a:m a:m e:m d e:m e:m
}

mel = \relative c' {
  \global
	 e4 ^\markup {\upright "Jede Strophe schneller"} b' | % 2
	 b  b | % 3
	 b8(  a)  g(  fis) | % 4
	 e4  e | % 5
	 g  g | % 6
	 fis  fis | % 7
	 e2 | % 8
	 e | % 9
	 e4  b' | % 10
	 b  b | % 11
	 b8(  a)  g(  fis) | % 12
	 e4  e | % 13
	 e8(  fis)  g4 | % 14
	 a  a | % 15
	 e(  c') | % 16
	 b(  a) | % 17
	 g  g | % 18
	 fis  fis | % 19
	 e2 | % 20
	 e 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Muss   al -- lein   zum   Wald      nun      geh  
	 -- en,   Le -- be -- wohl   dir   sa -- gen.   Muss   al  
	 -- lein   zum   Wald      nun      geh -- en,   Le  
	    -be -- wohl   dir   sa -- gen,   Le  
	 -- be -- wohl   dir   sa -- gen.  
}

melody = \relative c' {
  \global
	 e4  e | % 2
	 g  g | % 3
	 fis  d | % 4
	 e  e | % 5
	 e  e | % 6
	 b  b | % 7
	 e2 | % 8
	 e | % 9
	 e4  e | % 10
	 g  g | % 11
	 fis  d | % 12
	 e  e | % 13
	 e8(  d)  c4 | % 14
	 c  c | % 15
	 e(  e) | % 16
	 d(  c) | % 17
	 e  b | % 18
	 d  d | % 19
	 e2 | % 20
	 e 
	\bar "|."
}

melodyy = \relative c' {
  \global
  e'4 e |
  e e |
  d fis |
  e e |
  e e |
  b b | 
  e2 |
  e |
  e4 e |
  e e |
  d fis |
  e e |
  e8( d) c4 |
  c c |
  e2 |
  d4( c) |
  e b |
  d d |
  e2 |
  e
}

\book {
  
  \score {
    
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
     \new ChoirStaff <<
      \new Staff
      \with { \consists "Volta_engraver" } <<
      \new Voice = "first"
    { \voiceOne \mel }
  
     \addlyrics {\set fontSize = #+2 \verse}
     >>
    \new Staff
    \with { \consists "Volta_engraver" } << 
     
    \new Voice= "second"
    { \voiceTwo \melody }
    \new Voice= "three"
    { \voiceThree \melodyy }
>> >> >>
    \layout {
      
    \context {
      
      \Score  % \consists "Volta_engraver"
      \remove "Volta_engraver"
    }
    }
  }
}
