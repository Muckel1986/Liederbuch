\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 3/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 d:m d2:m c c4 f f2 bes bes4 d:m d2:m a a4 d:m d1:m d4:m c
  c2 c c4 f f2 d:m d4:m c c2 c c4 f f2 a:7 a4:7 d:m d2:m c c4 
  f f2 bes bes4 d:m d2:m a:m a4:m d:m
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    \partial 4
    a8 a |
    d4 f4. d8 |
    e4 g4. e8 |
    f4 a a |
    g8 f bes,4. g'8 |
    f e a,4. f'8 |
    e4 d c |
    d2 r4 | 
    r2 
  }
  d8 d |
  e4 g bes |
  bes2 c8 bes |
  bes a a4 f |
  d2 d8 d |
  e4 g bes |
  bes c4. bes8 |
  bes a a4 f |
  e2 a,8 a |
  d4 f4. d8 |
  e4 g4. e8 |
  f4 a a |
  g8 f bes,4. g'8 |
  f e a,4. f'8 |
  e4 d c |
  d2 r4 |
  r2 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Wenn de Wind dör de Böhm weiht un 
  Gras nich mehr was -- sen deit, un geel all ward, 
  denn kummt bald de Tied. Dat de Dach köt -- ter ward un de
  Nach, de du -- ert lang. Un de Kin -- ner von Na -- ber, de ward
  in Düs -- tern bang. Wenn de Reg'n vun't Reit -- dack 
  dröppt, min Söhn bu -- ten gau -- er löppt, sunst ward
  he natt, denn snurrt bin de Katt.
}

verse = \lyricmode {
  Wenn de Storm ö -- ver't Feld geiht, wo lang schon keen Korn
  mehr steiht, un Mehl all ward, denn is bald so wiet.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






