\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key f \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d1:m d:m d:m d:m g:m g:m a d:m d:m d:m d:m d:m g:m g:m a d:m
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  d2 f4 e |
  d2 f4 e |
  d2 f4 a |
  d2 d |
  bes g |
  g4( a) bes g |
  f2 e |
  d1 |
  \repeat volta 2 {
    d'2 ^\markup {\upright  "Kehrvers"} d4 d |
    d a a r |
    d a d f |
    e d d2 |
    bes2 g4. g8 |
    g4( a) bes g |
    f2 e4. e8 |
    d1
  }
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Hur -- ra, nun zieht uns -- re Schar nach der Hei -- mat,
  Finn -- lands schö -- nen Ge -- sta -- den zu.
  Nor -- di -- sches Hei -- mat -- land, Finn -- land, un -- ser Va -- ter -- land,
  gibt's doch kein Land, das so schön ist, wie du!
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






