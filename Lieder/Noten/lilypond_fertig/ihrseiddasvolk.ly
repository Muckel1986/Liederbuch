\version "2.14.2"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  d2 a g d g d a1 d2 a g d g d a d
  d a g d g d a1 d2 a
  g d g d a d
}

mel = \relative c' {
  \global
  d'2 ^\markup {\upright  "Kehrvers"} cis |
  b a |
  b a |
  a4( b) cis( a) |
  d2 cis |
  b a |
  g4 a8 g fis4 g8 fis |
  e2 d | \break
  fis4 fis8 g a4 fis8 e8 |
  d b d e fis2 |
  g4 a8 g fis4 g8 fis |
  e2 a4( g) |
  fis fis8 g a4 fis8 e |
  d b d e fis2 |
  g4 a8 g  fis4 g8 fis|
  e2 d \bar "|."
}

vers = \lyricmode{
Hal -- le -- lu -- ja, hal -- le -- lu -- ja, hal -- le -- lu -- ja, hal -- le -- lu -- ja, hal -- le -- lu -- ja.
Hal -- le -- lu -- ja, hal -- le -- lu, hal -- le -- lu -- ja,
hal -- le -- lu -- ja, hal -- le -- lu -- ja, hal -- le -- lu -- ja,
hal -- le -- lu, hal -- le -- lu -- ja, hal -- le -- lu -- ja, hal -- le -- lu -- ja.
}

verse = \lyricmode {
  " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " 
  \set stanza = #"1. " Ihr seid das Volk, das der Herr sich aus -- er -- seh'n,
  seid ei -- nes Sin -- nes und Geis -- tes. Ihr seid ge -- tauft durch den
  Geist zu ei -- nem Leib. 
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
