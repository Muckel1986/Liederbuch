\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d4 g2 d c g e:m b:m e1:m g2 d c d e:m d e:m d4:7 d g1 a:m g2
}

mel = \relative c' {
  \global
  \partial 4
  b4 |
  b b d d |
  e e d d |
  e fis8( g) fis4 e8( d) |
  e2 r4 b |
  b b d d |
  e e fis d |
  g fis8( e) fis4 d |
  g fis8( e) fis4 \break
  \repeat volta 2 {
    \bar "|:"
    d4 |
    <b g'>2 <a a'>4 <g b'> |
    <c a'>2 <a g'>4 <d fis> |
    <b g'>2.
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Und ob der Sturm auch man -- che Nacht uns 
  um die Oh -- ren pfiff, als sei die Höl -- le los ge -- macht,
  wir hiel -- ten durch mit Mann und Fracht, und heu -- te noch 
  fährt un -- ser Schiff.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






