\version "2.14.2"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble 
  \time 6/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  s4 d1:m d2:m c c4 a:m a2:m d:m d1:m a:m a2:m d:m d1:m
  c2 c4 a:m a2:m d:m d4:m a:7 a2:7 d:m d1:m f f2 c c1 d:m d2:m
  a:m a1:m f f2 c c1 d2:m d:m a:7 d1:m
}

melody = \relative c' {
  \global
	 \partial 4 d4 | % 2
	 f2  g4  a4.(  b8)  a4 | % 3
	 g2  e4  c4.(  d8)  e4 | % 4
	 f2  d4  d(  c)  d | % 5
	 e2  c4(  a2)  d4 | % 6
	 f2  g4  a4.(  b8  a4) | % 7
	 g2  e4  c4.(  d8)  e4 | % 8
	 f4.  e8(  d4)  cis(  b)  cis | % 9
	 d2  d4  d2. \bar "||" | % 10
	 c' ^\markup {\upright  "Kehrvers"} c4.(  b8)  a4 | % 11
	 g2  e4  c4.(  d8  e4) | % 12
	 f2(  d4)  d4.(  c8)  d4 | % 13
	 e2  c4  a2. | % 14
	 c'  c4.  b8  a4 | % 15
	 g2  e4  c4.(  d8)  e4 | % 16
	 f2(  d4)  cis  b  cis | % 17
	 d2.  d2 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " A -- las, my love, you do me wrong to cast me off dis -- cour -- teously,
	 and I have loved you so long, de -- light -- ing in your com -- pa -- ny.
	 Greens -- leeves was all my joy, Greens -- leeves was my de -- light.
	 Greens -- leeves was my heart of gold and who but you has greens -- leeves.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}