#!/bin/bash

lilypond -dbackend=eps -dno-gs-load-fonts -dinclude-eps-fonts --pdf *.ly
rm *-1.pdf
rm *.eps
rm *.count
rm *.tex
rm *.texi
rm *.midi
