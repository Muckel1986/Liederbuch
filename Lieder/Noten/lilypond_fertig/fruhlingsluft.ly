\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e1:m g2 b4:7 e:m a1:m e:m e4:m g e:m g c2 e:m 
  a:m b4 b:7 e1:m g2 d:7 g2. d4:7 g2 d g2. 
  d4 g2 d g1 b2 b:7 e:m
}

mel = \relative c' {
  \global
  e4. b8 e4 g |
  b4. g8 fis4 e4 |
  e4. d8 e d e d |
  b2. b8 b |
  e4 d e d |
  c2 b |
  a4. a8 b4 b |
  e2. \bar "||" d8^\markup {\upright  "Kehrvers"} d |
  g2 a |
  g4 r r a8 a |
  b2 a |
  b4 r r a8 a |
  b2 a |
  b r4 r |
  b2 b,4 b |
  e4 r r2 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Früh -- lings -- luft und blau -- er Him -- mel,
  Mäd -- chen -- lach -- en hin -- term Haus. Doch wir Lands -- knecht 
  trot -- ten vor -- wärts, mach -- en uns nichts draus.
  Und die Trom -- mel lockt: di -- ri -- dum, di -- dum,
  di -- ri -- dum, di -- dum, Brü -- der -- lein kumm!
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






