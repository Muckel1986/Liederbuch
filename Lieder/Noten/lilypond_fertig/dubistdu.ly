\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={

  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  c1 a:m a:m e:m e:m g g c
  c f a:m d:m d:m g g c c a:m 
  e f f f c c c c g g c
}

mel = \relative c' {
  \global
  \repeat volta 2 {
	 r4 c c d |
	 e2 c'4. b8 |
	 c4. b8 a4 c |
	 b4. g8 g4 a | 
	 g4. g8 g4 a |
	 g d2. |
  }
  \alternative {
    {
      f4 f f g |
      a1 |
    }
    {
	 f4 f e d |
	 c1 \bar "||" |
    } 
  }
	
	r4 ^\markup {\upright  "Kehrvers"} e f g |
	a4. a8 a4. a8 |
	a4 e f g |
	g f f e |
	f1 |
	g4 g8 g4 g8 g g |
	g4 d e f |
	f e e d |
	e2 c4 d |
	e4. e8 e4 e |
	gis gis a b |
	d c c b |
	a2 r2 | 
	r c,4 d |
	e1( |
	e4) g g g |
	g1( |
	g2) e4 c |
	d( g2.)( |
	g4) g, a b |
	c1 
	\bar "|."
}

vers = \lyricmode{
	 \set stanza = #"1. " Ver -- giss es nie, dass du lebst war 
	 kei -- ne ei -- ge -- ne I -- dee, und dass du
	 at -- mest kein Ent -- schluss von dir.
	 _ _ _ _ _ Du bist ge -- wollt, kein Kind des Zu -- falls,
	 kei -- ne Lau -- ne der Na -- tur, ganz e -- gal,
	 ob du dein Le -- bens -- lied in Moll singst o -- der
	 Dur. Du bist ein Ge -- dan -- ke Got -- tes, ein gen --
	 ial -- er noch da -- zu, du bist du, das ist der Clou,
	 ja, der Clou, ja, du bist du.
}

verse = \lyricmode{
	Ver -- giss es nie, dass du lebst war ei -- nes an -- de -- ren
	I -- dee, und dass du at -- mest _ _ _ _ _ sein Ge -- schenk an dich.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" }
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}

