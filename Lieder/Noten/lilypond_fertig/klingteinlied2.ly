\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 3/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  e2:m e4:m c c2 d d4 a a2 e:m e4:m g g2 d d4 a a2 e:m e4:m c c2 g g4 a a2
  f1 f f g g g b:7 b:7 b:7
}

mel = \relative c' {
  \global
  e4. e8 fis4 |
  g4 g a |
  fis4. e8 d4 |
  e e b |
  e4. e8 fis4 |
  g4 r g8 g |
  fis4 e d |
  e r b |
  e e fis |
  g g a |
  b a g |
  fis e e8 e |
  a4 a a | 
  a a a |
  a a a |
  a a a |
  b b b |
  b b b |
  b b b |
  b b b |
  b b b |
  b b b |
  b b b |
  b2. \bar "||"
}

verse = \lyricmode {
  \set stanza = #"3. " Auf Krä -- mer -- see -- len, Ban -- kiers und Kriegs -- hel -- den,
  zum hö -- fi -- schen Tanz, wer nicht schwarz trägt, ist bunt.
  Ein frei -- es Ge -- sicht lacht im Rhyth -- mus der Mes -- ser,
  richt nach bren -- nen -- den Plan -- ken, Schweiß, Meer -- salz, 
  Fisch und mit A -- hoi, ei -- nem Kuss, ei -- nem Fluch
  und mit Glück geht es drei -- mal zum Teu -- fel und drei -- mal zu -- rück.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \verse}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






