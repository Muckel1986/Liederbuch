\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s1 a:m e:m c e:m a:m g c e:m
  a:m b c e:m a:m e:m a:m g2 a:m 
  a1:m a2:m e:m a:m e:m
  a:m e:m c e:m a:m e:m a:m e:m c1 g2 a:m
}

melody = \relative c' {
  \global
	r2  r4   e8  e | % 2
	 a(  g  a  b)  c4.  a8 | % 3
	 b4.(  c8)  b4.  b8 | % 4
	 c(  b) c(  d)  e4  c | % 5
	 b4.  c8  b2 | % 6
	 c8(  b  c  d)  e4.  a,8 | % 7
	 d(  c  d  e)  d4  d8  a | % 8
	 c4  c8  d  e4  c8  c | % 9
	 b(  a  b  c)  b4  b8  b | % 10
	 c4  c8  d  e  c4  c8 | % 11
	 d4  d8(  e)  f2 | % 12
	 c4  c8  d  e4  c8  c | % 13
	 b(  a  b  c)  b4.  a8 | % 14
	 c  c4  c8  c  b4  a8 | % 15
	 b4  b8  c  b2 | % 16
	 a4  a8  b  c4  b8  a | % 17
	 b4.  g8  a2 \bar "||" | % 18
	 r2 ^\markup {\upright  "Kehrvers"} r4   e8  e | % 19
	 a4(  c8)  a  g4.  g8 | % 20
	 a4  c8(  a)  g2 | % 21
	 a4  c8  a  g4  g8  g | % 22
	 c  c  c4  b  e,8  e | % 23
	 a4(  c8  a)  g4.  g8 | % 24
	 a  c4  a8  g4  g8  g | % 25
	 c  c4  c8  c  b  a4 | % 26
	 b8  a4  g8  a2 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Es   war   Mitter --    nacht   im   Feen -- wald, 
	    das   Licht      des      Mon -- des  
	 bleich   und   kalt.   Rauschender   Wind   in  
	 uralten   Bäu -- men   und   Ne -- bel -- licht  
	    voll   von   ver -- lorenen   Träu -- men. 
	    Da   sah   ich   sie   blü -- hen   am     
	 We -- ges --    rand:   Stern -- blu -- men   hell   wie   ein  
	 leuchtendes   Band.   Da   pack -- te   mich  
	 Grau -- en   mit   eis -- kal -- ter   Hand.   Wuss -- te  
	 doch   längst   je -- des   Kind   im   Land:   Wenn   im  
	 Feen --    wald   nachts   die   Stern -- blumen   blüh'n,  
	 wen -- de   dich   ab   und   ver -- su -- che   zu  
	 flieh'n.   Denn   die   Feen         dort,   die   ha -- ben 
	    kein   Herz,   ih -- re   Spie -- le   be -- rei -- ten 
	    nur   Trau -- er   und   Schmerz.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \set chordNameLowercaseMinor = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
	  