\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e1:m b:m c c e:m d c c e:m a:m c c g f b:7 b:7 e:m b:m c
}

mel = \relative c' {
  \global
  b'4 g8 g e4 b |
  fis'4. a8 g4 fis |
  e2 e |
  r e4 fis |
  g2 g |
  fis4 fis e4. d8 |
  e1 |
  r |
  b'4 b b b |
  a g fis e |
  g2 g |
  r g4 a |
  b2 b |
  a4 b c c |
  b1 |
  r2 b4 a |
  b2 g |
  fis4 d b fis' |
  e1 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " We -- ni -- ge wa -- ren es, die Stel -- lung
  nah -- men un -- term Him -- mel, um zur Stadt zu gehn; als sie 
  sin -- gend ihr -- es We -- ges kam -- en, blie -- ben vie -- le
  auf den Stei -- gen stehn, blie -- ben vie -- le auf den Stei -- gen
  stehn.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






