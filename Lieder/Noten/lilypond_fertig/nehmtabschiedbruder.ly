\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key e \major 
  \time 4/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 s4 e1 b:7 e a e b2:7 b4:7 e4 a2 a4 b4:7
 e2 e4 a4 e1 b:7 e a e b:7 a2 a4 b4:7 e1
}

melody = \relative c' {
   \global
	 b4 | % 2
	 e4.  dis8  e4  gis | % 3
	 fis4.  e8  fis4  gis | % 4
	 e4.  e8  gis4  b | % 5
	 cis2.  cis4 | % 6
	 b4.  gis8  gis4  e | % 7
	 fis4.  e8  fis4  gis | % 8
	 e4.  cis8  cis4  b | % 9
	 e2. \bar "||" \break cis'4 ^\markup {\upright  "Kehrvers"} | % 10
	 b4.  gis8  gis4  e | % 11
	 fis4.  e8  fis4  cis' | % 12
	 b4.  gis8  gis4  b | % 13
	 cis2.  cis4 | % 14
	 b4.  gis8  gis4  e | % 15
	 fis4.  e8  fis4  gis | % 16
	 e4.  cis8  cis4  b | % 17
	 e2. s4  
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Nehmt   Ab -- schied   Brü -- der,   un -- ge -- wiß   ist  
	 al -- le   Wie -- der -- kehr.   Die   Zu -- kunft   liegt  
	 in   Fins -- ter -- nis   und   macht   das   Herz   uns  
	 schwer.   Der   Him -- mel   wölbt   sich   ü -- bers   Land,  
	 a -- de,   auf   Wie -- der -- sehn.   Wir   ruh -- en  
	 all   in   Got -- tes   Hand,   lebt   wohl,   auf   Wie -- der -- sehn.  
}

\book {
  \score {
    << 
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout { }
  }
}