\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 d1 a d g2 a:7 d1 g d2 a:7 d
}

melody = \relative c' {
  \global
	 d4  d r8   d  d  d | % 2
	 e4  e  fis  fis | % 3
	 d  d r8   d  d  d | % 4
	 b4  b  a2 | % 5
	 d4  d r8   d  e  fis | % 6
	 g4  fis  e  d | % 7
	 a  d  d  cis | % 8
	 d2 r2  
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Dan -- ke   für   die -- sen   gu -- ten   Mor -- gen,  
	 dan -- ke   für   je -- den   neu -- en   Tag,   dan  
	 -- ke,   dass   ich   all   mei -- ne   Sor -- gen   auf  
	 dich   wer -- fen   mag.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
