\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \time 4/4
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  a1:m e a:m c2 g c1 g2 e a:m e:m a:m g c1 g2 e a:m e:m a:m
}

melody = \relative c' {
      \tempo 4 = 80
      \set midiInstrument = #"flute"
      \clef treble
      \key d 
      \major
      \time 4/4
      %\tempo 4 = 120
	 b4  fis'  b,8  fis'  fis  fis | % 2
	 ais,(  e')  e2 r4 | % 3
	 b  fis'  b,8  fis'  fis  e | % 4
	 fis2  e | % 5
	r8   d  d  fis  a  fis  e  d | % 6
	 a'2  ais | % 7
	 b4  fis8  fis  a  g  fis  e | % 8
	 b4.(  cis16  b  a4) r4  | % 9
	r8   d  d  fis  a  fis  e  d | % 10
	 a'2  ais | % 11
	 b4  fis8  fis  a  g  fis  e | % 12
	 b2 r2  
	\bar "|."
	
}

melodyII = \relative c' {
        \set midiInstrument = #"french horn"
	\key d 
	\major 
	\time 4/4
	%\tempo 4 = 120
	\clef "treble_8"
	 d4  d  d8  d  d  d | % 2
	 cis4  cis2 r4  | % 3
	 d  d  fis8  gis  ais  b | % 4
	 a2  a | % 5
	r8   d  d  d  d  a  g  fis | % 6
	 e2  fis | % 7
	r8   fis  fis  fis  g  g  e  e | % 8
	 d4.(  e16  d  cis4) r4  | % 9
	r8   d'  d  d  d  a  g  fis | % 10
	 e2  fis | % 11
	r8   fis  fis  fis  g  g  e  e | % 12
	 fis2 r2  
	\bar "|."
	
}

melodyIII = \relative c' {
        \set midiInstrument = #"fretless bass"
        \clef bass
	\key d \major 
	\time 4/4
	%\tempo 4 = 120
	 b,4  b  b8  b  b  b | % 2
	 ais4  ais2 r4  | % 3
	 b  b  b8  b  b  cis | % 4
	 d2 cis | % 5 
	r8   d  d  d  d  d  d  d | % 6
	 cis2  cis | % 7
	r8   d  d  d  e  e  cis  cis | % 8
	 b4.(  cis16  b  a4) r4  | % 9
	r8   d  d  d  d  d  d  d | % 10
	 cis2  fis, | % 11
	%r8   d'  
        d4 d8  d  cis  cis  cis  cis | % 12 für mehrstimmiges
	 b2 r2  
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " In   der   Stun -- de   vor   dem   An -- griff  
	 saß   ich   un -- ter   ei -- ner   Bir -- ke,   ge -- noss   
	 die   Ru -- he   vor   dem   Stur -- me,   schloss  
	 mei -- ne   Au -- gen   vor   der   Welt.        
	 Ge -- noss   die   Ru -- he   vor   dem   Stur -- me,   
	 schloss   mei -- ne   Au -- gen   vor   der  Welt.
}

verse = \lyricmode {
         \set stanza = #"2. " An jenem un -- heil --  
         vol -- len A -- bend träumt' ich mir, ich wär al -- lei -- ne,
         trä -- umt' ich mir ich wär' al -- lei -- ne auf mei -- ner gro -- ßen, grau -- en Mähr',
         trä -- umt' ich mir ich wär' al -- lei -- ne auf mei -- ner gro -- ßen, grau -- en Mähr'.
}

versee = \lyricmode {
          \set stanza = #"3. " Plötz -- lich dräng -- ten dun -- kle Wol -- ken und es kam ein Sturm -- wind a -- uf,
          ri -- ss mir mei -- ne schwar -- ze Kap -- pe von mei -- nem un -- glücks -- sel' -- gem Haupt,
          ri -- ss mir mei -- ne schwar -- ze Kap -- pe von mei -- nem un -- glücks -- sel' -- gem Haupt.
}

verseee = \lyricmode {
          \set stanza = #"4. " Ei -- ner mei -- ner Of -- fi -- zie -- re, Traum -- deuter war er, sag -- te mi -- ir,
           di -- e -- ser Traum wür -- de be -- deu -- ten: Heut kost -- et's mei -- nen ar -- men Kopf,
           di -- e -- ser Traum wür -- de be -- deu -- ten: Heut kost -- et's mei -- nen ar -- men Kopf.
}

verseeee = \lyricmode {
          \set stanza = #"5. " In der Stun -- de vor dem An -- griff saß ich un -- ter ei -- ner Bir -- ke,
          u -- nd war un -- ruhig vor dem Stur -- me, schloss mei -- ne Au -- gen vor der Welt,
          u -- nd war un -- ruhig vor dem Stur -- me, schloss mei -- ne Au -- gen vor der Welt.
}
          
\book {
  \score {
    <<
      \new ChordNames {\germanChords \set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+1 \vers}
      \new Staff {\melodyII}
      \addlyrics {\set fontSize = #+1 \vers}
      \new Staff {\melodyIII}
    >>
    \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
    \layout { }
  }
}