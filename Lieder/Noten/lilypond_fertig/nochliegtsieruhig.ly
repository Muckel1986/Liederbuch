\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 e2:m e4:m d g2 d e1:m d a:m d c2 
  d e1:m e2:m e4:m d g2 d e1:m d
  a:m d c2 d e:m 
}

mel = \relative c' {
  \global
  \partial 4
  b4 |
  e4. fis8 g4 a |
  b b a d8 d |
  b2. g8 g |
  g( a) a2. |
  a4 a a fis8 g |
  a4 fis8( e) d4 e8 fis |
  g4 g fis d |
  e8 e e4 r b |
  e4. fis8 g4 a |
  b b a d8 d |
  b2. g8 g |
  g( a) a2 r8 \bar "||" \break
  g8 ^\markup {\upright  "Kehrvers"} |
  a4 a8 a a4 fis8 g |
  a4 fis8 e d4 e8 fis |
  g4 g fis d |
  e e r \bar "|."

}

vers = \lyricmode {
  \set stanza = #"1. " Noch liegt sie ruhig am Ha -- fen -- kai,
  heave a -- way, San -- ti -- a -- no, sech -- zig Mann, ich bin auch da -- bei,
  auf dem Drei -- mast -- kahn San -- ti -- a -- no.
  Let's go for Ca -- li -- for -- ni -- o, heave a -- way, San -- ti -- a -- no.
  Sie jagt vor dem Wind, vor -- wärts hin pfeil -- ge -- schwind 
  bis zum gold -- nen Port von Fris -- co.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
    \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






