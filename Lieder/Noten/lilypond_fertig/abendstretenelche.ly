\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  \set chordNameLowercaseMinor = ##t
  \override ChordName #'font-series = #'bold
  \germanChords
  a1:m a:m d:m a:m a:m a:m e a:m
  d:m a:m d:m a:m a:m a:m e a:m

}

mel = \relative c' {
  \global
  a2 c |
  e2. e4 |
  f f c d |
  e2 e |
  a, c |
  e2. f4 |
  e d c b |
  a1 | \break
  \repeat volta 2 {
  f'2 f |
  e e |
  a4 a f d |
  e2 e | 
  a, c |
  e2. f4 |
  e d c b |
  a1 |
}
}

vers = \lyricmode {
\set stanza = #"1. " A -- bends tre -- ten El -- che aus den Dü -- nen, zie -- hen von der Pal -- ve an den Strand,
wenn die Nacht wie ei -- ne gu -- te Mut -- ter lei -- se deckt ihr Tuch auf Haff und Land.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}

