\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key d \major
  \time 4/4
  %\tempo 4=90
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d2 a4 d |
  g2 d |
  b:m a |
  d1 |
  fis2:m g4 a |
  g2 d |
  fis:m a |
  d
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  d4^\markup {\upright  "1"} d e fis |
  g( fis8 e) d2 |
  fis4 b a e |
  fis2. r4 |
  a4^\markup {\upright  "2"} fis g a |
  b( a8 g ) fis2 |
  \set melismaBusyProperties = #'()
  \slurDown
  \slurDashed
  a4( fis e) a, |
  \unset melismaBusyProperties
  d2. r4 \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Bach und Flüs -- se star -- ren un -- term Ei -- se nun.
  Schnee und Er -- de knar -- ren un -- ter uns -- ren Schuhn.
}

verse = \lyricmode {
  \set stanza = #"2. " Tief im Fich -- ten -- wal -- de stäubt der Schnee und rinnt.
  Ü -- ber Hang und Hal -- de pfeift _ _ der Wind.
}

versee = \lyricmode {
  \set stanza = #"3. " Grü -- nes Vo -- gel -- sin -- gen ist im Frost ver -- hallt.
  Schwar -- ze Krä -- hen -- schwin -- gen pflü -- gen rau den Wald.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
          \addlyrics {\set fontSize = #+2 \verse}
          \addlyrics {\set fontSize = #+2 \versee}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






