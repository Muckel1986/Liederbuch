\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4 
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 e1:m b1:m c2 c4 d4 e1:m e:m b:m c2 c4 d4 g1 a:m d 
  g c a:m b:m c2 c4 d e1:m d4 d8 g2 g8 g1 d g d4 d8 
  e8:m e2:m e1:m c4 c8 d2 d8 
  \set chordChanges = ##f
  e1:m e:m
  \set chordChanges = ##t
}

melody = \relative c' {
  \global
  \partial 4
	 b8  b | % 2
	 e4  b'  a  g | % 3
	 fis  d r4   e8  fis | % 4
	 g4  fis  e  d | % 5
	 b r4  r8   b  b  b | % 6
	 e4  b'  a  g | % 7
	 fis  d r4   d8  d | % 8
	 e4  fis  g  a | % 9
	 b r2   b8  b | % 10
	 c4  b  a  g | % 11
	 fis  a r4   d,8  d | % 12
	 b'4  a  g  d | % 13
	 e r2   e8  e | % 14
	 a4  g  fis  e | % 15
	 d  fis r4   b,8  b | % 16
	 c4  e  g  fis | % 17
	 e
	\bar "||" \break
	 e ^\markup {\upright  "Kehrvers"} fis  g 
	\repeat volta 2 {
	 a4.  b8 ~   b2 | % 20
	r4   b  a  g | % 21
	 a  a8  a4  d  b8 ~  | % 22
	 b4  b  a  g | % 23
	 a4.  g8 ~   g2 | % 24
	r4   e  e  fis | % 25
	 g  e8  e4  d  e8 ~  
	}
	\alternative {
	{
	 e4  e  fis  g 
	}
	
	{
	 e2. r4  
	}}
	
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Wie   ein   Fest   nach   lan -- ger   Trau -- er,   wie  
	 ein   Feu -- er   in   der   Nacht,   ein   off -- nes  
	 Tor   in   ein -- er   Mau -- er,   für   die   Son  
	 -- ne   auf -- ge -- macht.   Wie   ein   Brief   nach   lan  
	 -- gem   Schwei -- gen,   wie   ein   un -- ver -- hoff -- ter  
	 Gruß,   wie   ein   Blatt   an   to -- ten   Zwei -- gen,  
	 ein    'Ich   mag   dich   trotz -- dem'  -- Kuss.   So   ist  
	 Ver -- söh -- nung,   so   muss   der   wah -- re   Frie  
	 -- den   sein,   so   ist   Ver -- söh -- nung,   so   ist  
	 Ver -- ge -- ben   und   Ver -- zeihn.   So   ist   Ver-  
	 -- zeihn.  
}

\book {
  \score {
    <<  
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
