\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s1 a:m a:m g g f f e e a:m a:m g g f f e
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    r4 a'8 a b a a g |
    a4 ( c2.) |
    r8 a a a b a a g |
    g4 ( b2.) |
    r4 g8 g a g g f |
    f4 ( a2.) |
    r4 f8 f f f g f |
    e1
  } \break
  r8 ^\markup {\upright  "Kehrvers"}
  e' e e e8. d8. c8 |
  c8. a16 ( a2. ) |
  r8 d d d d8. c8. b8 |
  b8. g16 ( g2. ) |
  r4 g8 a b8. a8. g8 |
  f8. f8. f8 f8. f8. g8 |
  a8. a8. a8 a8. b8. a8 |
  gis8. b16 ( b2. ) \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Wenn die Son -- ne sich schon senkt,
  ge -- schieht es ein ums and -- re Mal, wer -- den Lam -- pen 
  auf -- ge -- hängt in dem Dorf in Por -- tu -- gal. 
  Und sie be -- gin -- nen zu tan -- zen, das Dorf be -- wegt 
  sich im Gan -- zen, selbst die Kin -- der und Grei -- se auf
  ei -- ge -- ne Wei -- se, sie dreh'n sich im Krei -- se.
}

verse = \lyricmode {
  \set stanza = #"2. " Und wie schon in al -- ter Zeit 
  _ kommt ein je -- der oh -- ne Hatz,
  die Gi -- tar -- ren sind be -- reit,
  schon er -- füllt Mu -- sik den Platz.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






