\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  \set chordNameLowercaseMinor = ##t
  \override ChordName #'font-series = #'bold
  \germanChords
  s4. a1:m d g b:7 e:m c b:7 e2:m
}

mel = \relative c' {
  \global
  \partial 4.
  r8 ^\markup {\upright  "Bridge"} e e |
  e4 e e e8 e |
  fis fis4 fis8 fis4 fis8 fis |
  g g4 g8 g g4 g8 |
  fis2. g8 fis |
  e8 e4 d8 e4 e8 d |
  c c4 d8 e4 c8 c |
  b b4 b8 g' fis4 fis8 |
  e2 r8 \bar "||"
}

vers = \lyricmode {
  Doch mit je -- dem Tag, den der Bug so zer -- teilt schwämmt die See uns Ge -- wiss -- heit ins Blut, 
  die wir so Mal zu Mal mit den Win -- den ge -- eilt, gibt die Son -- ne den nö -- ti -- gen Mut.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics { \set fontSize = #+2 \vers }
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






