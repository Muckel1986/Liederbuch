\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  b2:7 e:m b:7 e:m d1 e4:m d b2:7 g1 c2 g a:m g d c e:m
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    fis4. a8 g( fis) e4 | 
    fis4. a8 g( fis) e4 |
    r d8 d d e fis g |
    e e d d b4 b |
  } \break
  \repeat volta 2 {
    g'4. a8 b4 b |
    c c b8( a) g4 |
  }
  a b8( c) d4 g, |
  g fis g2 |
  e r \bar "|."
}

vers = \lyricmode {
  \set stanza = #"3. " Mei -- ne Ner -- ven zit -- tern lei -- se
  im Ge -- dan -- ken an die Tü -- ren, die noch sper -- ren;
  an die Feu -- er, an die Feu -- er, die mich bit -- ten, sie zu schü -- ren.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






