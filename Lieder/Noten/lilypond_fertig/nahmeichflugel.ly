\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key d \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  b1:m g d e g2. a4 d1 b:m fis 
  b:m e2:m a b1:m e2:m fis
  g1 fis e:m fis e:m b:m a b:m
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  \repeat volta 2 {
    d'8 cis cis cis4 b b8 |
    \set melismaBusyProperties = #'()
    \slurDown
    \slurDashed
    b( b) a a4 g g8 |
    fis4 fis8( fis) fis e d4 |
    e1 |
    b8 b cis d4( d8) e d |
    fis fis g a4( a8) a a |
    b4 b8 b b b( b b ) |
    cis1 |
    \slurSolid
    \slurNeutral
    \unset melismaBusyProperties
  }
  \break
  \repeat volta 2 {
    r4^\markup {\upright  "Kehrvers"} d d cis |
    cis b b a |
    b1 |
    r2 r |
  }
  r4 b cis d |
  cis ais8 fis( fis2 ) |
  r4 e e8 fis g4 |
  fis1 |
  r4 e fis g |
  fis d8 b( b2) |
  r4 cis cis8 b a4 |
  b1 | \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Näh -- me ich Flü -- gel der 
  Mor -- _ gen -- rö -- te und blie -- be am äus -- sers -- ten Meer,
  wür -- de auch dort _ Dei -- ne Hand mit mir sein und Dei -- ne
  Rech -- te mich hal -- ten, _ _ Herr.
  Denn Fins -- ter -- nis ist Licht bei Dir,
  Denn du er -- forschst mein Herz und siehst 
  mei -- nen Sinn.
  Nur du kennst mei -- nen Weg und weißt, wer ich bin.
}
verse = \lyricmode {
  Sprä -- che ich ''Fins -- ter -- nis mö -- ge 
  mich de -- cken und Nacht statt _ Licht um mich sein'',
  wä -- re auch Fins -- ter -- nis nicht fins -- ter bei Dir
  _ und die Nacht leuch -- tet wie der Son -- nen -- schein.
  und des -- halb dank -- ich dir da -- für.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
          \addlyrics {\set fontSize = #+2 \verse}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






