\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  %\tempo 4 = 130
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e1:m e4:m d g d g1 g d c2 d e1:m e4:m d g1 
  c2 c4 g c c2 g4 c1 e:m a:m e2.:m d4 c1 d e:m
}

mel = \relative c' {
  \global
  
  \set midiInstrument = "flute"
  e2 e( |
  e4) fis g a |
  b2 b |
  r d4 b |
  a2 a4. a8 |
  e4 g fis d |
  e2 e4. r8 |
  e fis g a b4 b |
  b4. r8  c c c c |
  c b g4 c8 c c c |
  c b a4 c2( |
  c4) b g2( |
  g4) e a2( |
  a4) g e2( |
  e4) d e2 |
  g fis( |
  fis4) d e2 |
  e \bar "||" 
  <<
    \voiceOne {
    e8^\markup {\upright  "Zwischenspiel"} fis g a |
    b g fis g fis d e r \bar "|."
    }
    \new TabVoice
   {
     \set midiInstrument = "acoustic guitar (steel)"
  e,8 fis g a |
  b g fis g fis d e r \bar "|."
   }
  >>
}

vers = \lyricmode {
  \set stanza = #"1. " Früh -- lig dringt in den Nor -- den,
  Berg und Tal sind ein Blü -- ten -- meer ge -- wor -- den.
  Letz -- tes Eis treibt auf dem Fluss, Vo -- gel -- wer -- ben 
  bie -- tet dar die Ge -- burt wie je -- des Jahr,
  El -- che stehn im hel -- len Grün, im Früh -- ling
  hoch im Nor -- den.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






