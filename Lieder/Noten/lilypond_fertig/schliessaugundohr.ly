\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 d2:m f g:m d:m a1:7 d:m d2:m f a d:m a1:7 d2:m
}

mel = \relative c' {
  \global
  \partial 4
  d4 |
  d d8( e) f4 a |
  bes g a4. a8 |
  a4 g f e |
  d2. a'4 |
  a f8( g) a4 f |
  e g f4. f8 |
  a4 g f e |
  d2. \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Schließ Aug und Ohr für ei -- ne Weil
  vor dem Ge -- tös der Zeit. Du heilst es nicht und hast kein Heil,
  als wo dein Herz sich weiht.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






