\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 3/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 c2. e2.:m f2. c2. c2. e2.:m f2. c2. a2.:m e2.:m f2. c2. f2. e2.:m a2.:m g2. f2. e2:m f4 c
}

mel = \relative c' {
  \global
  \partial 4
  g4 |
  e'8 d e d e f |
  g2 e4 |
  c4. a8 c d |
  e2 g,4 |
  e'8 d e d e f |
  g2 e4 |
  c4. a8 c d |
  e2 g4 |
  c8 c c c b a |
  b2 c4 |
  a4. g8 a g |
  g2 g4 |
  a8 a a b a g |
  g2. |
  a2. |
  g2. |
  <a c>2. |
  <g b>4. <e g>8 <f a> <f a> |
  <e g>2 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Schon wird uns oft ums Herz zu eng,
  es lässt uns nie -- mals ruhn. Wir konn -- ten manch -- mal 
  im Ge -- dräng nicht ganz das Rech -- te tun. 
  Lasst in der Run -- de gehn den Wein, horcht, wie die Zeit
  ver -- rinnt; die Men -- schen wer -- den klei -- ner sein,
  wenn wir, wenn wir ge -- gan -- gen sind.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






