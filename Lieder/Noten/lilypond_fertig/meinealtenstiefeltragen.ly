\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a1:m e:m a:m e2:m g c1 f2 c d1:m e f2 c f c
}

mel = \relative c' {
  \global
  a4 a c a |
  e' e d b |
  a4. b8 c4 a |
  e'4. e8 d2 |
  c4 c e c |
  a'4. a8 g4 e |
  f4. e8 d c b a |
  e'2 e4 r |
  a a g e |
  a4. a8 g2 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Mei -- ne al -- ten Stie -- fel tra -- gen
  auf dem le -- der grau -- en Staub. Vor mir hol -- pern schwe -- re 
  Wa -- gen, und die kal -- ten Win -- de ja -- gen wir -- belnd durch das
  lo -- se Laub.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






