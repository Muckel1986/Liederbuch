\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 3/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 f1 f2 c2 c4 f1 f f4 c2 c4 f2 s4 c2 c4 f2 f4 c2 c4 f2 f4 d2:m d4:m c2 c4 f2 s4
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 a'4 | % 2
	 a  f  g | % 3
	 a2  f4 | % 4
	 g4.  f8  e4 | % 5
	 f2 r4  | % 6
	 a  a  bes | % 7
	 c2  a4 | % 8
	 g2  g4 | % 9
	 a2 r4  
	}
	 c ^\markup {\upright  "Kehrvers"}  c  g | % 11
	 a  f r4  | % 12
	 c'  c  g | % 13
	 a2 r4  | % 14
	 f2  a4 | % 15
	 g4.  f8  e4 | % 16
	 < a, f' >2 r4  
	\bar "|."
}

vers = \lyricmode{
	 \set stanza = #"1. " Wir   sa -- gen   euch   an   den   lie -- ben   Ad  
	 -- vent.   Se -- het,   die   er -- ste   Ker -- ze   brennt!  
	 Freut   euch,   ihr   Chri -- sten,   freu -- et   euch   sehr!  
	 Schon   ist   na -- he   der   Herr.  
}
verse = \lyricmode{
	 Wir   sa -- gen   euch   an   eine   hei -- li -- ge  
	 Zeit.   Ma -- chet   dem   Herrn   den   Weg   be -- reit.  
}

mel = \relative c' {
      \global
	\repeat volta 2 {
	 < f c' >4 | % 2
	 < f c' >  < a c >  < f b > | % 3
	 < f c' >2  < a c >4 | % 4
	 < d, b' >4.  < a' c >8  < g c >4 | % 5
	 < a c >2 r4  | % 6
	 < f c' >  < f c' >  < d g > | % 7
	 < e a >2  < f c' >4 | % 8
	 < e c' >2  < e c' >4 | % 9
	 < f c' >2 r4  
	}
	 < e g c >  < d f c' >  < c f a > | % 11
	 < c f a >  < a c f > r4  | % 12
	 < e' g c >  < d g c >  < c e g > | % 13
	 < c e a >2 r4  | % 14
	 < a d f >2  < c f a >4 | % 15
	 < b d g >4.  < a c f >8  < g e' >4 | % 16
	 < a f' >2 r4  
	\bar "|."
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
      \new Staff {\mel}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}