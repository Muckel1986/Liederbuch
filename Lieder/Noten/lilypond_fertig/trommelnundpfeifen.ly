\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
 f1 c b c:7 f c f2 c f c:7
 f1 c:7 d2:m g:m f4 c:7 f2 f4 c:7 f2
}

mel = \relative c' {
  \global
	\repeat volta 2 {
	 f4  c8  c  a'4  g8  f | % 2
	 e4.  g8  c,4.  c8 | % 3
	 d4.  e8  f(  e)  d4 | % 4
	 c(  c'  ces)  bes | % 5
	 a4.  c8  bes(  a  g)  f | % 6
	 g4  e  c4.  c8 | % 7
	 f4  f  g  g | % 8
	 c  bes8(  a)  g2 
	\bar "||"
	| % 9
	 f8 ^\markup {\upright  "Kehrvers"} f  a  f  c'  c  bes  a | % 10
	 g  f  e  f  g4(  a) | % 11
	 d,8  d  f  d  bes'  bes  a  g 
	}
	\alternative {
	  
	{
	 f  f  a  bes  c(  bes  a  g) 
	}
	
	{
	| % 13
	 f  c  a'  bes  < a c >2 
	}}
	
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Trom -- meln   und   Pfei -- fen   mit   hel -- lem   Klang  
	 ziehn   da   den   Weg      ent -- lang.        
	 Nach   Ma -- ri -- gna -- no   zie -- hen  
	 sie,   ja   di -- ri -- di -- ri   di -- ri     
	 di   Trom -- mel -- fel -- le   be -- ben   in   das  
	 lich -- te   Mor -- gen -- rot      Pfei -- fen   sind  
	 das   Le -- ben   und   die   Trom -- meln   sind   der  
	 Tod.            Trom -- meln   sind   der   Tod.  
	
}
melody = \relative c' {
  \global
	\repeat volta 2 {
	 f4  c8  c  a'4  g8  f | % 2
	 e4.  g8  c,4.  c8 | % 3
	 d4.  e8  f(  e)  d4 | % 4
	 c2.  c4 | % 5
	 f4.  a8  g(  f  e)  d | % 6
	 g,4  g  c4.  c8 | % 7
	 f4  e8  d  c  d  e4 | % 8
	 a  g8(  f)  c2 
	\bar "||"
	| % 9
	 f8  f  f  f  a  a  g  f | % 10
	 e  d  c  d  e2 | % 11
	 d8  d  d  d  g  g  g  g 
	}
	\alternative {
	{
	 f  f  e  e  f2 
	}
	
	{
	| % 13
	 f8  c  a'  g  f2 
	}}
	
	\bar "|."
	
}

\book {
  
  \score {
    
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff  
      \with { \consists "Volta_engraver" } <<
      \new Voice = "first"
    { \voiceOne \mel }
  
     \addlyrics {\set fontSize = #+2 \verse}
    \new Voice= "second"
    { \voiceTwo \melody }
     >> >>
    \layout {
      
    \context {
      
      \Score  % \consists "Volta_engraver"
      \remove "Volta_engraver"
    }
    }
  }
}
