\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key f \major 
  \time 6/8 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 d2:m d4:m a2:m a4:m f2 f4 d2:m d1:m a2:m a4:m f2 f4 d2:m d1:m c2 c4 f2 f4 d2:m d1:m c2 c4 f2 f4 d:m
}

melody = \relative c' {
  \global
	 a8  c | % 2
	 d4  d  d8  c | % 3
	 a4  a  a8  c | % 4
	 f4  f  f8  e | % 5
	 d4. r8   a  c | % 6
	 d4  d  d8  c | % 7
	 a4  a  a8  a | % 8
	 c4  c  bes8  bes | % 9
	 a4. r8   a  c | % 10
	 d4  a  d8  f | % 11
	 e4  c  e8  g | % 12
	 f4  f  g8  g | % 13
	 a4. r8   a  bes | % 14
	 a4  f  f8  a | % 15
	 g4  e  e8  g | % 16
	 f4  d  e8  c | % 17
	 f4. s8  s4  
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Als   der   A -- bend   sich   still   leg -- te   auf  
	 die   Fel -- der,   auf   die   Höh'n   stand   al -- lein  
	 ich   an   der   Klip -- pe,   hab   ins   tie -- fe  
	 Tal   ge -- seh'n.   Ne -- bel -- strei -- fen   wie   Ge  
	 -- spen -- ster   war   ihr   Glanz   in   A -- bend -- glut,  
	 ein -- ge -- taucht   im   letz -- ten   Schein   der   Son  
	 -- ne   Glanz   wie   Blut   so   rot.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
