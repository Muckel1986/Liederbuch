\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  e1:m g e:m g a:m e:m a2:m b:7 e1:m e:m d d:7 b:7 g2 d g a:m b1:7 e:m
}

mel = \relative c' {
  \global
  g'8. fis16 e8 fis g fis e g |
  b8. a16 g8 a b16 b b4. |
  g8. fis16 e8 fis g fis e g |
  b8. a16 g8 a b16 b b4. |
  \repeat volta 2 {
    c8 c c c d c16 d8. c8 |
    b b b b b a16 b8. g8 |
    c c c c b16 a b4 g8 |
    e4 e e2 
  } \break
  e4. ^\markup {\upright  "Kehrvers"} e8 b4 e |
  d4. e8 d4 e |
  fis4. e8 d4 e |
  b4 b b b |
  b'4. g8 a4 d |
  b4. g8 a4 a |
  b4. a8 g4 fis |
  e e e r \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Drau -- ßen war -- ten A -- ben -- teu -- er; uns -- re See -- len bren -- nen heiß!
  In die Käl -- te steigt das Feu -- er, man -- che vol -- le Fla -- sche kreist.
  Kei -- ner kann zu Hau -- se blei -- ben, drau -- ßen nur sich rum -- zu -- trei -- ben,
  ist es, was uns im -- mer noch am Le -- ben hält.
  Son -- nen -- schein und wil -- de Fes -- te sind im Le -- ben noch das Bes -- te!
  Und der Hen -- ker kriegt die Res -- te, was vom Lum -- pen üb -- rig blieb.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






