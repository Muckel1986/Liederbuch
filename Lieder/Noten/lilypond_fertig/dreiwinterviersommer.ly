\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s8 d2:m f c c d:m f bes c d:m c bes a d:m c a:7 d4:m
}

mel = \relative c' {
  \global
  \partial 8 
  c8 |
  d( a') a g |
  a4 g8 f |
  g( a) g e |
  c4. c8 |
  d a' a g |
  a4 g8 g |
  f d f4 |
  g4. \bar "||"
  a8 ^\markup {\upright  "Kehrvers"} |
  d,4 d8 d |
  e4 e8 e |
  f e d e( |
  e4 ) a |
  d, d8 d |
  g4 g8 f |
  e d cis4 |
  d4. \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Drei Win -- ter, vier
  Som -- mer, drei Äp -- fel am Baum, jetzt
  komm ich zu meim Schatz aus dem Ar -- beits
  -- haus heim. Vom Spinn -- rad, vom Spinn --
  rad, vom Spinn -- rad, tri -- ra; vom Spinn
  -- rad, vom Spinn -- rad, vom Spinn -- rad,
  halts an.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






