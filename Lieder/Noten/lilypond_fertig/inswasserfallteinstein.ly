\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major 
  \time 4/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \germanChords
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
 s4 d1 fis:m g a a d fis:m g a a g2 a d1 g2 a fis4:m fis8:m b8:m b2:m
 g2 d g d g a4 a8 d8 d2 
}

melody = \relative c' {
  \global
	 a4 | % 2
	 fis'4.  e8  fis4  g | % 3
	 a2.  fis4 | % 4
	 d4.  cis8  d4  g | % 5
	 fis(  e8)  e(  e2 | % 6
	 e2.)  a,4 | % 7
	 fis'4.  e8  fis4  g | % 8
	 a2.  fis4 | % 9
	 d4.  cis8  d4  g | % 10
	 fis(  e8)  e(  e2 | % 11
	 e2.)  d4 | % 12
	 b'4.  a8  b4  cis | % 13
	 d2  a | % 14
	r4   b  a  g | % 15
	 a  a8  d,(  d4) r8   d | % 16
	 g4  fis  d4.  d8 | % 17
	 g4  fis  d4.  d8 | % 18
	 g4  fis  e  e8  d( | % 19
	 d2.) s4  
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Ins   Was -- ser   fällt   ein   Stein,   ganz   heim -- lich  
	 still   und   lei -- se, und   ist  
	 er   noch   so   klein,   er   zieht   doch   wei -- te  
	 Krei -- se.      Wo   Got -- tes   gro  
	 -- ße   Lie -- be   in   ei -- nen   Men -- schen   fällt,  
	    da   wirkt   sie   fort,   in   Tat   und   Wort  
	 hi -- naus   in   uns -- re   Welt.     
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}
