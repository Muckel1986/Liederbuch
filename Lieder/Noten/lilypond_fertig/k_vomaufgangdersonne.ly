\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble 
  \time 4/4
  \partial 64*16
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 c4 ^\markup {\upright  "1"} | % 2
	 c2  e4  g | % 3
	 c2  c | % 4
	 g4 ^\markup {\upright  "2"} c  g  e | % 5
	 c4.  c8  c4  c8  d | % 6
	 e4 ^\markup {\upright  "3"} e8  e  e(  d)  c  d | % 7
	 e2.  e8  f | % 8
	 g4 ^\markup {\upright  "4"} g8  g  g(  c)  b  a | % 9
	 g4.
	}
	
}

verse = \lyricmode{
	 Vom   Auf -- gang   der   Son -- ne   bis   zu   ihr  
	 -- em   Nie -- der -- gang   sei   ge -- lo -- bet   der  
	 Na -- me      des   Herrn,   sei   ge -- lo -- bet  
	 der   Na -- me      des   Herrn!  
}

\book {
  \score {
    <<
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}