\version "2.12.3"

%carmiina nerothana s. 33

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordName = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  c2 g a:m e:m f c g:7 c c g a:m e:m f c g:7 c c c c g:7 c g c g4:7 c g1
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a2:m g c f c g:7 c
}

mel = \relative c' {
  \global
  e4 g g d |
  c e e b |
  a c c e |
  g f e2 |
  e4 g g d |
  c e e b |
  a c c e |
  d d c2 |
  <<
      {
        \voiceOne
        \bar "|:" s2 ^\markup {\upright  "Kehrvers"} e4 e |
        g g a b |
        c c b a |
        g g f e |
        d2 a4 b |
      }
      \new Voice {
        \voiceTwo
        \bar "|:" s2 e4 e |
        g g g g |
        a g g f |
        e e d c |
        g2 r |
      }
    >>
}

melo = \relative c' {
  \global
  c4 c d d |
  e g a b |
  c g g f |
  e2 \bar ":|"
}

meloo = \relative c' {
  r4 a8 a g4 g |
  c c f f |
  e e d8( c) d4 |
  c2 \bar ":|"
}

vers = \lyricmode {
  \set stanza = #"1. " Du machst Klein -- holz, ich such Rin -- de, soll ein Feu -- er
  hier ent -- stehn, dass ich Harz und Spä -- ne fin -- de, mö -- ge bald und schnell 
  ge -- schehn. Ist die Hor -- de schon im Kom -- men, eilt da -- her auf stei -- lem
  Pfad, wird ihr
}

verse = \lyricmode {
  wohl ein Es -- sen from -- men nach dem lan -- gen vol -- len Tag.
}

versee = \lyricmode {
  wird ihr wohl ein Es -- sen from -- men nach dem lan -- gen Tag.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordName}
      \new Staff \with {
	\consists "Volta_engraver" 
      } <<
        \new Voice = "first"
        { \mel }
        \addlyrics {\set fontSize = #+2 \vers}
     >> >>
     
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
  
  \score {
   \new StaffGroup <<
     \set StaffGroup.systemStartDelimiter = #'SystemStartSquare
    \new ChordNames {\set chordChanges = ##t \chordNames}
    \new Staff <<
      \new Voice = "RefrainA" {
        \melo
      }
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \new Staff <<
      \new Voice = "RefrainB" {
        \meloo
      }
      \addlyrics {\set fontSize = #+2 \versee}
    >>
  >>
  
  \layout {
    ragged-last = ##f
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






