\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key d \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  b1:m e2:m b:m e:m b:m fis b:m b1:m 
  a2 d e:m b:m fis b:m b1:m fis:m g d e:m b:m fis b:m
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  fis4 fis fis4. fis8 |
  e8. fis16 g8 e fis2 |
  e8. fis16 g8 e d4 d |
  cis8 e d cis b2 |
  fis'4 b d4. b8 |
  a8. b16 a8 g fis2 |
  e8 fis g e d4 d |
  cis8 e d cis b2 |
  b'4 b d b |
  a8 b4 a8 cis4 a |
  g8 g4 g8 b4 g |
  fis8. g16 fis8 e fis2 |
  e4 e g e |
  d8 e4 d8 fis4 d |
  cis4 fis fis ais, |
  cis8 b b ais b2 |
  \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Nachts steht Hun -- ger starr in un -- serm Traum, tags die Schüs -- se knal -- len, her vom Wal -- des -- saum.
  E -- lend hält mit den Ko -- lon -- nen Schritt und in Frost und Ne -- bel ziehn die Wöl -- fe mit.
  Noch fliegt Russ -- lands hei -- li -- ger Ad -- ler, Müt -- ter -- chen, un -- ser Blut ge -- hört nur dir,
  mag das ro -- te Heer uns auch ja -- gen, leuch -- tend steht noch im -- mer das Pa -- nier.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






