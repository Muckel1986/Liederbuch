\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 g2 c g d e:m d g d g d e:m b:7 c g d b:7 d g
}

mel = \relative c' {
  \global
  \partial 4
  d4 |
  g g g fis8( e) |
  d4. d8 d4 e8( fis) |
  g4 g a a |
  b2 a4 
  \repeat volta 2 {
    a |
    b b8( c) d4 a |
    g g8( a) b4 fis |
    e e8( fis) g4 b |
  }
  \alternative {
    { a2 b4 s }
    { a2 g4 s \bar "|." }
  }
}

melo = \relative c' {
  \global
  \partial 4
   d4 |
   b c d c |
   b4. g8 a4 c |
   b b d d |
   g,( b) d 
  \repeat volta 2 {
    fis |
    g g fis d |
    e e dis b |
    c c b g' |
  }
  \alternative {
    { d( fis) b, s }
    { g'( fis) g s \bar "|." }
  }
}

vers = \lyricmode{
 \set stanza = #"1. " Der lang ge -- nug mit viel Be -- dacht des Hau -- ses Haft er -- tra -- gen,
 hat ü -- ber Nacht sich auf -- ge -- macht, die gro -- ße Fahrt zu wa -- gen,
 wa -- gen.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        } <<
	\new Voice = "first"
	  {\voiceOne \mel}
	\new Voice= "second"
	  {\voiceTwo \melo}
	
      \addlyrics {\set fontSize = #+2 \vers}
  >> >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
