\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  a2:m f g c a: f g c a:m f g c a:m f g c
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 c2 ^\markup {\upright  "1"}  c8  c  c  c | % 2
	 d2  e\fermata | % 3
	 e4 ^\markup {\upright  "2"}  e  f  f | % 4
	 g(  f)  g2\fermata | % 5
	 a ^\markup {\upright  "3"}  a | % 6
	 b8  b  a  b  c4  c8\fermata  b | % 7
	 a4 ^\markup {\upright  "4"}  g  f  e | % 8
	 d2  c\fermata 
	}
	
}

verse = \lyricmode{
	 Herr,   gib   uns   dei -- nen   Frie -- den,   gib   uns  
	 dei -- nen   Frie -- den,      Frie -- den,   gib   uns  
	 dei -- nen   Frie -- den,   Herr,   gib   uns   dei -- nen  
	 Frie -- den.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
