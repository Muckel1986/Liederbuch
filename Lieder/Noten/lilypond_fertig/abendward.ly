\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
 c1 f4 g4 c2 f g c1
 c1. g2 c g c1

}

mel = \relative c' {
  \global
 c4 d e c8( g) |
 a4 b c2 |
 f4 e d d |
 e1 |
 e4 f g4. g8 |
 f4 e d2 |
 g,4 c d d |
 c1 \bar "|." |
}

vers = \lyricmode {
\set stanza = #"1. " A -- bend ward, bald kommt die Nacht, schla -- fen geht die Welt,
denn sie weiß, es ist die Wacht ü -- ber ihr be -- stellt.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}

