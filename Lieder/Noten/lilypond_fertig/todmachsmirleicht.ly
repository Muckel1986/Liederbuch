\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d4:m c8 d1:m d2:m c d1:m d2:m c f 
  c d:m a:m a:m d4:m d8:m c d1:m d8:m c8 c4 d2:m
}

mel = \relative c' {
  \global
  \partial 4.
  d8 d c |
  d2 r4 d8 e |
  f d e f g g e c |
  d2 r8 a d e |
  f d e f g4 f8 g |
  a a bes a g4 a8 g |
  f d g f e c a4( |
  a2 ) r8 d d c |
  d2 r4 d8 e |
  f e d c d2 |
  r2 r8 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Tod mach's mir leicht.
  Wenn du kommst vor dei -- ner Zeit, Tod mach's mir leicht.
  Mein Weg im Le -- ben ist noch weit.
  Mit der Waf -- fe in der Hand schickt man mich in dies und
  je -- nes Land. Tod mach's mir leicht, denn ich bin noch 
  nicht be -- reit.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






