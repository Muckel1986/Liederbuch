\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 d2:m c d1:m d2:m c f1 g2:m a d1:m a a 
  d2:m c d1:m d2:m c f1 g2:m a d1:m a a 
  g1:m d:m a d:m a d:m g:m d:m g:m d:m g:m d:m a d2:m
}

mel = \relative c' {
  \global
  \partial 4
  a'4 |
  d,4. e8 f e d e |
  d2 r4 a |
  d4. e8 f e f g |
  a2 r4 a |
  g bes a cis |
  d a g f |
  e e f e | 
  a2 r4 a |
  d,4. e8 f e d e |
  d2 r4 a |
  d4. e8 f e f g |
  a2 r4 a |
  g bes a cis |
  d a g f |
  e e f e |
  a2 r4 \bar "||" \break
  a4^\markup {\upright  "Kehrvers"} |
  g8. g16 g8 g g g f e |
  f8. d16 d8 d d r d d |
  a8. a16 a8 a cis cis cis cis |
  d8. cis16 d8 e f r f f |
  a, a a a cis cis cis cis |
  d4 d r d |
  g8. g16 g8 g g g f e |
  f8. d16 d8 d d r d d |
  g8. g16 g8 g g g f g |
  a16 a8. a8 a a r a a |
  g8. g16 g8 g f f g g |
  a4 a r a8 a |
  g4. g8 f4 e |
  d d r \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Man sagt, im Win -- ter ist es kalt, und je -- der Mensch wird ein -- mal alt.
  Wir wolln bei Mut -- ter Er -- de blei -- ben, ist es auch mal kalt.
  Zwölf Ster -- ne sind auf eu -- rem Schein. Ihr Wert ist für uns wirk -- lich klein.
  Wir bau -- en uns -- re Häu -- ser sel -- ber. Je -- des stürzt mal ein.
  Wir rei -- ten o -- der lau -- fen, uns -- re Fel -- der sind nicht klein, und wir wis -- sen sel -- ber,
  wie er ist, der hel -- le Son -- nen -- schein, und wir le -- ben sel -- ber wei -- ter, wei -- ter, wei -- ter.
  Wir ha -- ben uns -- re eig -- nen Lie -- der, ei -- ge -- ne Gra -- vur, pfei -- fen le -- bens -- froh auf
  eu -- re Eh -- re, Treu -- e, Pflicht und Schwur, nur der Mond ist un -- ser ein -- zig treu -- er Lei -- ter.
  Un -- ser Herz sei im -- mer hei -- ter.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






