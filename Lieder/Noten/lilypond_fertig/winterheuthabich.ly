\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 6/8
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d2. g4. e4.:m d2. a2. d4.  
  b1:m b2:m d4. a4. d4. g2. b2.:m b4.:m a4.
  d4. g4. b4.:m a4. b2.:m d4. a4. d2. 
}

mel = \relative c' {
  \global
  \repeat volta 2 {  
    fis8 ^\markup {\upright  "Kehrvers"} 
    a8. a16 a8 g fis |
    g fis8. e16 b4 d8 |
    d fis fis fis e d |
  }
  \alternative {
    {
      e8. fis16 e8 cis a4 
    }
    {
      cis8 b a b4. ( |
      b4.) \bar "||"
    }
  }
  r4 b8 |
  b b b d8. d16 d8 |
  e d cis d16 a8. a8 |
  b g g g a g |
  fis4. ( fis4 ) fis8 |
  d' cis b e d cis |
  fis e d g fis8. e16 |
  d8 d d cis b a |
  b4. ( b4 ) b8 |
  d cis b e d cis |
  fis4. ( fis4. ) \bar "|."
}

vers = \lyricmode {
  Win --  ter, heut hab ich dich tan -- zen ge -- sehn.
  Ans Fens -- ter -- glas lock -- en mich tan -- zen -- de Flock -- en.
  _ _ _ _ 
  \set stanza = #"1. " Es wirkt, als ein Geist und der Marsch wei -- ter
  Fen -- nen mit schloh -- wei -- ßen Tü -- chern be -- deckt.
  Schon scheint es, als wenn sie zu schla -- fen be -- gän -- nen
  und wür -- den nie wie -- der ge -- weckt. Das hat mich oft an dir
  er -- schreckt.
}

verse = \lyricmode {
  Wir -- beln so schwung -- voll und tan -- zen so schön, deine
  Flo -- cken, als wür -- den sie _ _ _ _ _ nie mehr ver -- gehn.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






