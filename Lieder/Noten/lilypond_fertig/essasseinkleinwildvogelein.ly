\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key g \major 
  \time 3/4
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s8 e1:m e2:m b:7 b4:7 e:m e2:m
  g g4 d d2 e:m e8:m b:7 e2:m
}

melody = \relative c' {
  \global
  \partial 8
  b8 |
  e8. e16 e4. fis8 |
  g8. fis16 e4. d8 |
  b' b b,4 b |
  e e \break
  \repeat volta 2 {
    r8 b' |
    b8. a16 g4. b8 |
    a8. g16 fis4. fis8 |
    e8. fis16 g4. dis8 |
    e4 e r8
  }
}

verse = \lyricmode{
 \set stanza = #"1. " Es saß ein klein, wild Vö -- ge -- lein auf ei -- nem grü -- nen Äst -- chen.
 Es sang die gan -- ze Win -- ter -- nacht, sein Stimm tät laut er -- klin -- gen.
}

\book {
  \score {
    << 
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}  
    >>
    \layout { }
  }
}
